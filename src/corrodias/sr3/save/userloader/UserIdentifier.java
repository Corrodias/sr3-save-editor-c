package corrodias.sr3.save.userloader;

import corrodias.sr3.save.data.User;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

public class UserIdentifier {

	private static final Pattern WHITESPACE_PATTERN = Pattern.compile("\\s+");
	private static final Pattern LINEBREAK_PATTERN = Pattern.compile("[\\r\\n]+");

	public List<User> resolve(String steamDir) throws Exception {
		File dir = new File(steamDir + File.separatorChar + "userdata");
		if (!dir.isDirectory()) {
			throw new FileNotFoundException();
		}
		File[] subdirs = listSubdirs(dir);
		List<User> result = new ArrayList<>(10);
		for (File subdir : subdirs) {
			try {
				User user = resolve(steamDir, subdir.getName());
				result.add(user);
			} catch (Exception ignored) {
			}
		}
		return result;
	}

	private File[] listSubdirs(File dir) {
		return dir.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pathname.isDirectory();
			}
		});
	}

	@SuppressWarnings("FeatureEnvy")
	public User resolve(String steamDir, String userDir) throws Exception {
		if ((steamDir == null) || steamDir.isEmpty()
				|| (userDir == null) || userDir.isEmpty()) {
			throw new FileNotFoundException();
		}
		File file = new File(steamDir + File.separatorChar + "userdata" + File.separatorChar + userDir + File.separatorChar + "config" + File.separatorChar + "localconfig.vdf");
		if (!file.isFile()) {
			throw new FileNotFoundException();
		}

		try (Scanner sc = new Scanner(file)) {
			sc.useDelimiter(LINEBREAK_PATTERN);
			while (sc.hasNext()) {
				String line = sc.next();
				List<String> split = getStrings(line);
				if ((split.size() >= 2) && "\"PersonaName\"".equals(split.get(0))) {
					return new User().setName(split.get(1)).setSteamid(userDir).setSavePath(file.getPath());
				}
			}
		} catch (FileNotFoundException ignored) {
		}
		return null;
	}

	private List<String> getStrings(CharSequence line) {
		String[] split = WHITESPACE_PATTERN.split(line);
		List<String> result = new ArrayList<>(split.length);
		for (String s : split) {
			if ((s != null) && !s.isEmpty()) {
				result.add(s);
			}
		}
		return result;
	}
}
