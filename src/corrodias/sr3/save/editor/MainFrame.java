package corrodias.sr3.save.editor;

import corrodias.sr3.garage.manager.GarageEditor;
import corrodias.sr3.save.data.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@SuppressWarnings("MagicNumber")
public class MainFrame extends JFrame {

	private static final BigDecimal ONE_HUNDRED = new BigDecimal(100);
	private static final int WINDOW_WIDTH = 1024;
	private static final int WINDOW_HEIGHT = 768;
	private static final Dimension SPACER = new Dimension(0, 10);
	@SuppressWarnings({"NumericCastThatLosesPrecision", "UnnecessaryExplicitNumericCast"})
	private static final Dimension BUTTON_SIZE = new Dimension(100, (int) new JButton("Button").getSize().getHeight());
	private Sr3SaveFile saveFile;
	private File lastSaveFilePath;
	private File lastAppearanceFilePath;
	@SuppressWarnings("FieldCanBeLocal")
	private JButton loadButton;
	private JButton saveButton;
	private JButton importAppearanceButton;
	private JButton exportAppearanceButton;
	private JButton garageButton;
	private JButton newGamePlusButton;
	private JTabbedPane tabbedPane;
	private JPanel playerPanel;
	private JFormattedTextField moneyField;
	private JSpinner respectLevelField;
	private JFormattedTextField luchadoresNotorietyDecayRateMultiplierField;
	private JFormattedTextField deckersNotorietyDecayRateMultiplierField;
	private JFormattedTextField morningstarNotorietyDecayRateMultiplierField;
	private JFormattedTextField policeNotorietyDecayRateMultiplierField;
	private JFormattedTextField bulletDamageTakenMultiplierField;
	private JFormattedTextField explosiveDamageTakenMultiplierField;
	private JFormattedTextField vehicleDamageTakenMultiplierField;
	private JFormattedTextField fireDamageTakenMultiplierField;
	private JFormattedTextField fallingDamageTakenMultiplierField;
	private JFormattedTextField healthRegenMultiplierField;
	private JFormattedTextField respectMultiplierField;
	//	private JFormattedTextField meleeDamageMultiplierField;
	private JPanel weaponsPanel;
	private List<JCheckBox> weaponUnlockCheckBoxes;
	private List<JSpinner> weaponUpgradeLevelSpinners;
	private JPanel upgradesPanel;
	private List<JCheckBox> upgradeUnlockCheckBoxes;
	private List<JCheckBox> upgradeAvailabilityCheckBoxes;
	private JPanel homiesPanel;
	private List<JCheckBox> homiesUnlockCheckBoxes;
	private List<JCheckBox> dlcHomiesUnlockCheckBoxes;
	private JPanel territoriesPanel;
	private Map<District, JPanel> districtSubPanels;
	private Map<Neighborhood, JPanel> neighborhoodSubPanels;
	private List<JCheckBox> territoriesOwnedCheckBoxes;
	private ListReader lists;

	public MainFrame(ListReader listReader) {
		super("SR3 Save Editor C v" + Main.MAJOR_VERSION + '.' + Main.MINOR_VERSION);

		lists = listReader;
		lastSaveFilePath = Main.getSavePath();
		lastAppearanceFilePath = lastSaveFilePath;
		weaponUnlockCheckBoxes = new ArrayList<>(10);
		weaponUpgradeLevelSpinners = new ArrayList<>(10);
		upgradeUnlockCheckBoxes = new ArrayList<>(10);
		upgradeAvailabilityCheckBoxes = new ArrayList<>(10);
		homiesUnlockCheckBoxes = new ArrayList<>(10);
		dlcHomiesUnlockCheckBoxes = new ArrayList<>(10);
		territoriesOwnedCheckBoxes = new ArrayList<>(10);
		districtSubPanels = new EnumMap<>(District.class);
		neighborhoodSubPanels = new EnumMap<>(Neighborhood.class);

		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));

		addEditPanel();
		addButtonPanel();

		setUiEnabledAfterLoading(false);
	}

	private static JFormattedTextField createFloatField() throws NumberFormatException {
		JFormattedTextField newField = new JFormattedTextField(new Float("0.0"));
		DefaultFormatter fmt = new NumberFormatter(new DecimalFormat("#,##0.00"));
		fmt.setValueClass(newField.getValue().getClass());
		DefaultFormatterFactory fmtFactory = new DefaultFormatterFactory(fmt, fmt, fmt);
		newField.setFormatterFactory(fmtFactory);
		Dimension textSize = new Dimension(100, 100);
		newField.setMaximumSize(textSize);
		newField.setMinimumSize(textSize);
		return newField;
	}

	private static void addVerticalGlueToAll(Iterable<JPanel> panels) {
		for (JPanel panel : panels) {
			panel.add(Box.createVerticalGlue());
		}
	}

	private static ActionListener makeEventHandlerThatTogglesAllContainedCheckboxes(final JCheckBox eventThrower, final JPanel togglingPanel) {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				setAllCheckBoxesSelected(togglingPanel, eventThrower.isSelected());
			}

			private void setAllCheckBoxesSelected(JComponent component, boolean selected) {
				for (Component subComponent : component.getComponents()) {
					if (subComponent instanceof JCheckBox) {
						((AbstractButton) subComponent).setSelected(selected);
					}
					if (subComponent instanceof JComponent) {
						setAllCheckBoxesSelected((JComponent) subComponent, selected);
					}
				}
			}
		};
	}

	private static void setAllComponentsEnabled(JComponent jc, boolean enabled) {
		for (Component component : jc.getComponents()) {
			try {
				JComponent jComponent = (JComponent) component;
				jComponent.setEnabled(enabled);
				setAllComponentsEnabled(jComponent, enabled);
			} catch (ClassCastException ignored) {
			}
		}
	}

	private static void setAllComponentsAlignmentXToValue(JComponent jc, float alignment) {
		for (Component component : jc.getComponents()) {
			try {
				JComponent jComponent = (JComponent) component;
				jComponent.setAlignmentX(alignment);
				setAllComponentsAlignmentXToValue(jComponent, alignment);
			} catch (ClassCastException ignored) {
			}
		}
	}

	private void addEditPanel() {
		tabbedPane = new JTabbedPane();
		getContentPane().add(tabbedPane);

		addPlayerPanel();
		addWeaponsPanel();
		addUpgradesPanel();
		addHomiesPanel();
		addTerritoriesPanel();
	}

	private void addPlayerPanel() {
		playerPanel = new JPanel();
		playerPanel.setLayout(new BoxLayout(playerPanel, BoxLayout.Y_AXIS));
		playerPanel.setBorder(new EmptyBorder(4, 4, 4, 4));
		tabbedPane.addTab("Player", playerPanel);

		addPlayerMoneyArea();
		playerPanel.add(Box.createRigidArea(SPACER));
		addPlayerRespectArea();
		playerPanel.add(Box.createRigidArea(SPACER));
		addPlayerMultiplierArea();
		playerPanel.add(Box.createVerticalGlue());
		setAllComponentsAlignmentXToValue(playerPanel, LEFT_ALIGNMENT);
	}

	private void addPlayerMoneyArea() {
		playerPanel.add(new JLabel("Money"));
		moneyField = new JFormattedTextField(new BigDecimal("0.00"));
		DefaultFormatter fmt = new NumberFormatter(new DecimalFormat("#,##0.00"));
		fmt.setValueClass(moneyField.getValue().getClass());
		DefaultFormatterFactory fmtFactory = new DefaultFormatterFactory(fmt, fmt, fmt);
		moneyField.setFormatterFactory(fmtFactory);
		Dimension textSize = new Dimension(100, 100);
		moneyField.setMaximumSize(textSize);
		moneyField.setMinimumSize(textSize);
		playerPanel.add(moneyField);
	}

	private void addPlayerRespectArea() {
		playerPanel.add(new JLabel("Respect Level"));
		respectLevelField = new JSpinner(new SpinnerNumberModel(1, 1, 50, 1));
		Dimension textSize = new Dimension(100, 100);
		respectLevelField.setMaximumSize(textSize);
		respectLevelField.setMinimumSize(textSize);
		playerPanel.add(respectLevelField);
	}

	@SuppressWarnings("OverlyLongMethod")
	private void addPlayerMultiplierArea() {
		JPanel multiplierPanel = new JPanel();
		multiplierPanel.setLayout(new BoxLayout(multiplierPanel, BoxLayout.Y_AXIS));
		multiplierPanel.setBorder(new TitledBorder("Multipliers"));
		playerPanel.add(multiplierPanel);

		multiplierPanel.add(Box.createRigidArea(SPACER));
		multiplierPanel.add(new JLabel("Luchadores Notoriety Decay Rate"));
		luchadoresNotorietyDecayRateMultiplierField = createFloatField();
		multiplierPanel.add(createTooltipQuestionPanel(luchadoresNotorietyDecayRateMultiplierField, "<html>Default: 1.00<br/>Level 1: 1.10<br/>Level 2: 1.20<br/>Level 3: 1.30</html>"));

		multiplierPanel.add(Box.createRigidArea(SPACER));
		multiplierPanel.add(new JLabel("Deckers Notoriety Decay Rate"));
		deckersNotorietyDecayRateMultiplierField = createFloatField();
		multiplierPanel.add(createTooltipQuestionPanel(deckersNotorietyDecayRateMultiplierField, "<html>Default: 1.00<br/>Level 1: 1.10<br/>Level 2: 1.20<br/>Level 3: 1.30</html>"));

		multiplierPanel.add(Box.createRigidArea(SPACER));
		multiplierPanel.add(new JLabel("Morningstar Notoriety Decay Rate"));
		morningstarNotorietyDecayRateMultiplierField = createFloatField();
		multiplierPanel.add(createTooltipQuestionPanel(morningstarNotorietyDecayRateMultiplierField, "<html>Default: 1.00<br/>Level 1: 1.10<br/>Level 2: 1.20<br/>Level 3: 1.30</html>"));

		multiplierPanel.add(Box.createRigidArea(SPACER));
		multiplierPanel.add(new JLabel("Police Notoriety Decay Rate"));
		policeNotorietyDecayRateMultiplierField = createFloatField();
		multiplierPanel.add(createTooltipQuestionPanel(policeNotorietyDecayRateMultiplierField, "<html>Default: 1.00<br/>Level 1: 1.10<br/>Level 2: 1.20<br/>Level 3: 1.30</html>"));

		multiplierPanel.add(Box.createRigidArea(SPACER));
		multiplierPanel.add(new JLabel("Bullet Damage Taken"));
		bulletDamageTakenMultiplierField = createFloatField();
		multiplierPanel.add(createTooltipQuestionPanel(bulletDamageTakenMultiplierField, "<html>Default: 1.00<br/>Level 1: 0.95<br/>Level 2: 0.90<br/>Level 3: 0.80<br/>Level 4: 0.00 (immune)</html>"));

		multiplierPanel.add(Box.createRigidArea(SPACER));
		multiplierPanel.add(new JLabel("Explosive Damage Taken"));
		explosiveDamageTakenMultiplierField = createFloatField();
		multiplierPanel.add(createTooltipQuestionPanel(explosiveDamageTakenMultiplierField, "<html>Default: 1.00<br/>Level 1: 0.95<br/>Level 2: 0.90<br/>Level 3: 0.80<br/>Level 4: 0.00 (immune)</html>"));

		multiplierPanel.add(Box.createRigidArea(SPACER));
		multiplierPanel.add(new JLabel("Vehicle Damage Taken"));
		vehicleDamageTakenMultiplierField = createFloatField();
		multiplierPanel.add(createTooltipQuestionPanel(vehicleDamageTakenMultiplierField, "<html>Default: 1.00<br/>Level 1: 0.90<br/>Level 2: 0.70<br/>Level 3: 0.50<br/>Level 4: 0.00 (immune)</html>"));

		multiplierPanel.add(Box.createRigidArea(SPACER));
		multiplierPanel.add(new JLabel("Fire Damage Taken"));
		fireDamageTakenMultiplierField = createFloatField();
		multiplierPanel.add(createTooltipQuestionPanel(fireDamageTakenMultiplierField, "<html>Default: 1.00<br/>Level 1: 0.90<br/>Level 2: 0.70<br/>Level 3: 0.50<br/>Level 4: 0.00 (immune)</html>"));

		multiplierPanel.add(Box.createRigidArea(SPACER));
		multiplierPanel.add(new JLabel("Falling Damage Taken"));
		fallingDamageTakenMultiplierField = createFloatField();
		multiplierPanel.add(createTooltipQuestionPanel(fallingDamageTakenMultiplierField, "<html>Default: 1.00<br/>Level 1: 0.90<br/>Level 2: 0.70<br/>Level 3: 0.50<br/>Level 4: 0.00 (immune)</html>"));

		multiplierPanel.add(Box.createRigidArea(SPACER));
		multiplierPanel.add(new JLabel("Health Regeneration Rate"));
		healthRegenMultiplierField = createFloatField();
		multiplierPanel.add(createTooltipQuestionPanel(healthRegenMultiplierField, "<html>Default: 1.00<br/>Level 1: 1.25<br/>Level 2: 1.50<br/>Level 3: 1.75<br/>Level 4: 2.00</html>"));

		multiplierPanel.add(Box.createRigidArea(SPACER));
		multiplierPanel.add(new JLabel("Respect Gained"));
		respectMultiplierField = createFloatField();
		multiplierPanel.add(createTooltipQuestionPanel(respectMultiplierField, "<html>Default: 1.00<br/>Level 1: 1.05<br/>Level 2: 1.10<br/>Level 3: 1.15<br/><br/>= Bonuses per Stronghold =<br/>Level 1: +0.05<br/>Level 2: +0.10<br/>Level 3: +0.15<br/><br/>Maximum: 1.60 = (1.15 +  3 × 0.15)</html>"));

		// we don't really understand how this one works, yet.
//		multiplierPanel.add(Box.createRigidArea(SPACER));
//		multiplierPanel.add(new JLabel("Melee Damage Dealt"));
//		meleeDamageMultiplierField = createFloatField();
//		multiplierPanel.add(meleeDamageMultiplierField);
	}

	private JPanel createTooltipQuestionPanel(Component child, String tooltip) {
		JPanel containerPanel = new JPanel();
		containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.X_AXIS));
//		containerPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		containerPanel.add(child);
		JComponent icon = new JLabel(new ImageIcon(getClass().getResource("/images/dialog_question.png")));
		icon.setToolTipText(tooltip);
		containerPanel.add(icon);
		return containerPanel;
	}

	private void addWeaponsPanel() {
		JPanel weaponsContainerPanel = new JPanel();
		weaponsContainerPanel.setLayout(new BoxLayout(weaponsContainerPanel, BoxLayout.Y_AXIS));

		JPanel infoPanel = new JPanel();
		infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));
		weaponsContainerPanel.add(infoPanel);
		infoPanel.add(new JLabel("  V - Level"));
		infoPanel.add(new JLabel("             V - Unlocked"));

		weaponsPanel = new JPanel();
		weaponsPanel.setLayout(new BoxLayout(weaponsPanel, BoxLayout.Y_AXIS));

		JScrollPane scrollPane = new CustomScrollPane(weaponsPanel);
		weaponsContainerPanel.add(scrollPane);

		tabbedPane.addTab("Weapons", weaponsContainerPanel);

		setAllComponentsAlignmentXToValue(weaponsContainerPanel, LEFT_ALIGNMENT);
	}

	private void readWeaponUpgrades() throws Exception {
		clearWeaponsPanel();
		populateWeaponsPanel();
		setWeaponUnlockCheckBoxStates();
		weaponsPanel.revalidate();
		weaponsPanel.repaint();
	}

	private void clearWeaponsPanel() {
		weaponsPanel.removeAll();
		weaponUnlockCheckBoxes.clear();
		weaponUpgradeLevelSpinners.clear();
	}

	private void populateWeaponsPanel() throws Exception {
		Map<Integer, Integer> weaponUpgradeLevels = getWeaponUpgradeLevels();

		List<String> weaponNames = lists.getWeaponNames();
		for (String name : weaponNames) {
			JPanel weaponPanel = new JPanel();
			weaponPanel.setLayout(new BoxLayout(weaponPanel, BoxLayout.X_AXIS));

			// weapon level
			Number level = weaponUpgradeLevels.get(StringHashes.hash(name));
			if (level == null) {
				level = 1;
			}
			JSpinner spinner = new JSpinner(new SpinnerNumberModel(level, 1, 4, 1));
			spinner.setMaximumSize(new Dimension(20, 20));
			weaponUpgradeLevelSpinners.add(spinner);
			weaponPanel.add(spinner);

			// weapon unlock
			JCheckBox checkBox = new JCheckBox(name);
			weaponUnlockCheckBoxes.add(checkBox);
			weaponPanel.add(checkBox);

			weaponPanel.add(Box.createHorizontalGlue());

			weaponsPanel.add(weaponPanel);
		}

		weaponsPanel.add(Box.createVerticalGlue());

		setAllComponentsAlignmentXToValue(weaponsPanel, LEFT_ALIGNMENT);
	}

	private void addUpgradesPanel() {
		JPanel upgradesContainerPanel = new JPanel();
		upgradesContainerPanel.setLayout(new BoxLayout(upgradesContainerPanel, BoxLayout.Y_AXIS));

		JPanel infoPanel = new JPanel();
		infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));
		upgradesContainerPanel.add(infoPanel);
		infoPanel.add(new JLabel("   V - Available"));
		infoPanel.add(new JLabel("          V - Unlocked"));

		upgradesPanel = new JPanel();
		upgradesPanel.setLayout(new BoxLayout(upgradesPanel, BoxLayout.Y_AXIS));

		JScrollPane scrollPane = new CustomScrollPane(upgradesPanel);
		upgradesContainerPanel.add(scrollPane);

		tabbedPane.addTab("Upgrades", upgradesContainerPanel);

		setAllComponentsAlignmentXToValue(upgradesContainerPanel, LEFT_ALIGNMENT);
	}

	private void readUpgrades() throws Exception {
		clearUpgradesPanel();
		populateUpgradesPanel();
		setUpgradeUnlockCheckBoxStates();
		setUpgradeAvailabilityCheckBoxStates();
		upgradesPanel.revalidate();
		upgradesPanel.repaint();
	}

	private void clearUpgradesPanel() {
		upgradesPanel.removeAll();
		upgradeUnlockCheckBoxes.clear();
		upgradeAvailabilityCheckBoxes.clear();
	}

	private void populateUpgradesPanel() {
		int[] upgradeHashes = saveFile.getUpgradeHashes();
		for (int hash : upgradeHashes) {
			String name = StringHashes.getString(hash);

			JPanel upgradePanel = new JPanel();
			upgradePanel.setLayout(new BoxLayout(upgradePanel, BoxLayout.X_AXIS));

			// upgrade availability
			JCheckBox availableCheckBox = new JCheckBox();
			upgradeAvailabilityCheckBoxes.add(availableCheckBox);
			upgradePanel.add(availableCheckBox);

			// upgrade unlock
			JCheckBox unlockedCheckBox = new JCheckBox(name);
			upgradeUnlockCheckBoxes.add(unlockedCheckBox);
			upgradePanel.add(unlockedCheckBox);

			upgradePanel.add(Box.createHorizontalGlue());

			// if the hash was 0, this entry is empty. hide it, but keep it in the lists so the status arrays still line up without further effort.
			if (hash != 0) {
				upgradesPanel.add(upgradePanel);
			}
		}

		upgradesPanel.add(Box.createVerticalGlue());

		setAllComponentsAlignmentXToValue(upgradesPanel, LEFT_ALIGNMENT);
	}

	private void addHomiesPanel() {
		JPanel homiesContainerPanel = new JPanel();
		homiesContainerPanel.setLayout(new BoxLayout(homiesContainerPanel, BoxLayout.Y_AXIS));

		JPanel infoPanel = new JPanel();
		infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));
		homiesContainerPanel.add(infoPanel);
		infoPanel.add(new JLabel("   V - Unlocked"));

		homiesPanel = new JPanel();
		homiesPanel.setLayout(new BoxLayout(homiesPanel, BoxLayout.Y_AXIS));

		JScrollPane scrollPane = new CustomScrollPane(homiesPanel);
		homiesContainerPanel.add(scrollPane);

		tabbedPane.addTab("Homies", homiesContainerPanel);

		setAllComponentsAlignmentXToValue(homiesContainerPanel, LEFT_ALIGNMENT);
	}

	private void readHomies() throws Exception {
		clearHomiesPanel();
		populateHomiesPanel();
		setHomiesUnlockCheckBoxStates();
		setDlcHomiesUnlockCheckBoxStates();
		homiesPanel.revalidate();
		homiesPanel.repaint();
	}

	private void clearHomiesPanel() {
		homiesPanel.removeAll();
		homiesUnlockCheckBoxes.clear();
		dlcHomiesUnlockCheckBoxes.clear();
	}

	private void populateHomiesPanel() {
		addVanillaHomies();
		homiesPanel.add(new JSeparator(SwingConstants.HORIZONTAL));
		addDlcHomies();
		homiesPanel.add(Box.createVerticalGlue());
		setAllComponentsAlignmentXToValue(homiesPanel, LEFT_ALIGNMENT);
	}

	private void addVanillaHomies() {
		int[] homiesHashes = saveFile.getHomieHashes();
		for (int hash : homiesHashes) {
			String name = StringHashes.getString(hash);

			JPanel homiePanel = new JPanel();
			homiePanel.setLayout(new BoxLayout(homiePanel, BoxLayout.X_AXIS));

			// homie unlock
			JCheckBox unlockedCheckBox = new JCheckBox(name);
			homiesUnlockCheckBoxes.add(unlockedCheckBox);
			homiePanel.add(unlockedCheckBox);

			homiePanel.add(Box.createHorizontalGlue());

			// if the hash was 0, this entry is empty. hide it, but keep it in the lists so the status arrays still line up without further effort.
			if (hash != 0) {
				homiesPanel.add(homiePanel);
			}
		}
	}

	private void addDlcHomies() {
		int[] dlcHomiesHashes = saveFile.getDlcHomieHashes();
		for (int hash : dlcHomiesHashes) {
			String name = StringHashes.getString(hash);

			JPanel homiePanel = new JPanel();
			homiePanel.setLayout(new BoxLayout(homiePanel, BoxLayout.X_AXIS));

			// homie unlock
			JCheckBox unlockedCheckBox = new JCheckBox(name);
			dlcHomiesUnlockCheckBoxes.add(unlockedCheckBox);
			homiePanel.add(unlockedCheckBox);

			homiePanel.add(Box.createHorizontalGlue());

			// if the hash was 0, this entry is empty. hide it, but keep it in the lists so the status arrays still line up without further effort.
			if (hash != 0) {
				homiesPanel.add(homiePanel);
			}
		}
	}

	private void addTerritoriesPanel() {
		territoriesPanel = new JPanel();
		territoriesPanel.setLayout(new BoxLayout(territoriesPanel, BoxLayout.Y_AXIS));

		JScrollPane scrollPane = new CustomScrollPane(territoriesPanel);
		tabbedPane.addTab("Territories", scrollPane);

		setAllComponentsAlignmentXToValue(territoriesPanel, LEFT_ALIGNMENT);
	}

	private void readTerritories() throws Exception {
		clearTerritoriesPanel();
		populateTerritoriesPanel();
		territoriesPanel.revalidate();
		territoriesPanel.repaint();
	}

	private void clearTerritoriesPanel() {
		territoriesPanel.removeAll();
		territoriesOwnedCheckBoxes.clear();
		districtSubPanels.clear();
	}

	private void populateTerritoriesPanel() {
		addDistrictPanelsToTerritoriesPanel();
		addNeighborhoodPanelsToDistrictPanels();
		addTerritoriesToNeighborhoodPanels();
		addVerticalGlueToAll(neighborhoodSubPanels.values());
		addVerticalGlueToAll(districtSubPanels.values());
		territoriesPanel.add(Box.createVerticalGlue());
		setAllComponentsAlignmentXToValue(territoriesPanel, LEFT_ALIGNMENT);
	}

	private void addDistrictPanelsToTerritoriesPanel() {
		for (District district : District.values()) {
			addToTerritoriesPanel(district);
		}
	}

	private void addToTerritoriesPanel(District district) {
		JPanel subpanel = new JPanel();
		subpanel.setLayout(new BoxLayout(subpanel, BoxLayout.Y_AXIS));

		JCheckBox title = new JCheckBox(district.toString());
		title.setFont(title.getFont().deriveFont(Font.BOLD, 16.0f));
		title.addActionListener(makeEventHandlerThatTogglesAllContainedCheckboxes(title, subpanel));
		subpanel.add(title);

		districtSubPanels.put(district, subpanel);
		territoriesPanel.add(subpanel);
		territoriesPanel.add(Box.createVerticalStrut(20));
	}

	private void addNeighborhoodPanelsToDistrictPanels() {
		for (District district : District.values()) {
			populateDistrictPanelWithNeighborhoods(district);
		}
	}

	private void populateDistrictPanelWithNeighborhoods(District district) {
		JPanel districtPanel = districtSubPanels.get(district);

		for (Neighborhood neighborhood : district.getNeighborhoods()) {
			addNeighborhoodPanelToDistrictPanel(districtPanel, neighborhood);
		}
	}

	private void addNeighborhoodPanelToDistrictPanel(JPanel districtPanel, Neighborhood neighborhood) {
		JPanel neighborhoodPanel = new JPanel();
		neighborhoodPanel.setLayout(new BoxLayout(neighborhoodPanel, BoxLayout.Y_AXIS));

		JPanel titleContainer = new JPanel();
		titleContainer.setLayout(new BoxLayout(titleContainer, BoxLayout.X_AXIS));
		neighborhoodPanel.add(titleContainer);

		JCheckBox title = new JCheckBox(neighborhood.toString());
		title.setFont(title.getFont().deriveFont(Font.BOLD, 12.0f));
		title.addActionListener(makeEventHandlerThatTogglesAllContainedCheckboxes(title, neighborhoodPanel));

		titleContainer.add(Box.createHorizontalStrut(20));
		titleContainer.add(title);

		neighborhoodSubPanels.put(neighborhood, neighborhoodPanel);
		districtPanel.add(neighborhoodPanel);
	}

	private void addTerritoriesToNeighborhoodPanels() {
		List<Territory> territories = lists.getTerritories();
		boolean[] territoryStatuses = saveFile.getTerritoryStatuses();
		for (int i = 0; i < territoryStatuses.length; i++) {
			Territory territory = territories.get(i);

			JPanel territoryPanel = new JPanel();
			territoryPanel.setLayout(new BoxLayout(territoryPanel, BoxLayout.X_AXIS));

			territoryPanel.add(Box.createHorizontalStrut(40));

			// territory ownership
			JCheckBox checkBox = new JCheckBox(territory.name);
			checkBox.setSelected(territoryStatuses[i]);
			territoriesOwnedCheckBoxes.add(checkBox);
			territoryPanel.add(checkBox);

			territoryPanel.add(Box.createHorizontalGlue());

			JPanel neighborhoodPanel = neighborhoodSubPanels.get(territory.neighborhood);
			neighborhoodPanel.add(territoryPanel);
		}
	}

	private void addButtonPanel() {
		JPanel buttonPanel = new JPanel();
		buttonPanel.setBorder(new EmptyBorder(4, 4, 4, 4));
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
		getContentPane().add(buttonPanel);
		addLoadButton(buttonPanel);
		addSaveButton(buttonPanel);
		buttonPanel.add(Box.createRigidArea(SPACER));
		addImportAppearanceButton(buttonPanel);
		addExportAppearanceButton(buttonPanel);
		buttonPanel.add(Box.createRigidArea(SPACER));
		addGarageButton(buttonPanel);
		buttonPanel.add(Box.createRigidArea(SPACER));
		addNewGamePlusButton(buttonPanel);
		buttonPanel.add(Box.createVerticalGlue());
	}

	private void addLoadButton(JPanel buttonPanel) {
		loadButton = new JButton("Load");
		loadButton.setMinimumSize(BUTTON_SIZE);
		loadButton.setMaximumSize(BUTTON_SIZE);
		loadButton.addActionListener(new LoadButtonListener());
		buttonPanel.add(loadButton);
	}

	private void addSaveButton(JPanel buttonPanel) {
		saveButton = new JButton("Save");
		saveButton.setMinimumSize(BUTTON_SIZE);
		saveButton.setMaximumSize(BUTTON_SIZE);
		saveButton.addActionListener(new SaveButtonListener());
		buttonPanel.add(saveButton);
	}

	private void addImportAppearanceButton(JPanel buttonPanel) {
		importAppearanceButton = new JButton("<html><center>Import<br/>Appearance</center></html>");
		importAppearanceButton.setMinimumSize(BUTTON_SIZE);
		importAppearanceButton.setMaximumSize(BUTTON_SIZE);
		importAppearanceButton.addActionListener(new ImportAppearanceButtonListener());
		buttonPanel.add(importAppearanceButton);
	}

	private void addExportAppearanceButton(JPanel buttonPanel) {
		exportAppearanceButton = new JButton("<html><center>Export<br/>Appearance</center></html>");
		exportAppearanceButton.setMinimumSize(BUTTON_SIZE);
		exportAppearanceButton.setMaximumSize(BUTTON_SIZE);
		exportAppearanceButton.addActionListener(new ExportAppearanceButtonListener());
		buttonPanel.add(exportAppearanceButton);
	}

	private void addGarageButton(JPanel buttonPanel) {
		garageButton = new JButton("Garage");
		garageButton.setMinimumSize(BUTTON_SIZE);
		garageButton.setMaximumSize(BUTTON_SIZE);
		garageButton.addActionListener(new GarageButtonListener());
		buttonPanel.add(garageButton);
	}

	private void addNewGamePlusButton(JPanel buttonPanel) {
		newGamePlusButton = new JButton("<html><center>New Game +</center></html>");
		newGamePlusButton.setMinimumSize(BUTTON_SIZE);
		newGamePlusButton.setMaximumSize(BUTTON_SIZE);
		newGamePlusButton.addActionListener(new NewGamePlusButtonListener());
		buttonPanel.add(newGamePlusButton);
	}

	private void removeCribUpgrades() {
		for (int i = 0; i < upgradeUnlockCheckBoxes.size(); i++) {
			JCheckBox upgradeBox = upgradeUnlockCheckBoxes.get(i);
			JCheckBox availableBox = upgradeAvailabilityCheckBoxes.get(i);
			if (upgradeBox.getText().startsWith("CRIB - ")) {
				upgradeBox.setSelected(false);
				availableBox.setSelected(false);
			}
		}
	}

	public void saveFile() throws Exception {
		JFileChooser chooser = new JFileChooser();
		if (lastSaveFilePath != null) {
			chooser.setCurrentDirectory(lastSaveFilePath.getParentFile());
			chooser.setSelectedFile(lastSaveFilePath);
		}
		chooser.setFileFilter(new FileNameExtensionFilter("SR3 save files", "sr3s_pc"));
		int returnVal = chooser.showSaveDialog(this);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			String filename = chooser.getSelectedFile().getPath();
			if (!filename.endsWith(".sr3s_pc")) {
				filename += ".sr3s_pc";
			}
			Sr3SaveFileIO.writeFile(saveFile, filename);

			lastSaveFilePath = chooser.getSelectedFile();
		}
	}

	@SuppressWarnings("ReturnOfNull")
	public Sr3SaveFile loadSaveFile() throws Exception {
		JFileChooser chooser = makeJFileChooserForSaveFiles();
		int returnVal = chooser.showOpenDialog(this);
		if (returnVal != JFileChooser.APPROVE_OPTION) {
			return null;
		}

		File selectedFile = chooser.getSelectedFile();
		return readSaveFile(selectedFile);
	}

	private JFileChooser makeJFileChooserForSaveFiles() {
		JFileChooser chooser = new JFileChooser();
		if (lastSaveFilePath != null) {
			chooser.setCurrentDirectory(lastSaveFilePath.getParentFile());
		}
		chooser.setFileFilter(new FileNameExtensionFilter("SR3 save files", "sr3s_pc"));
		return chooser;
	}

	private Sr3SaveFile readSaveFile(File selectedFile) throws Exception {
		Sr3SaveFile newFile = Sr3SaveFileIO.readFile(selectedFile);
		lastSaveFilePath = selectedFile;
		return newFile;
	}

	public void exportAppearance() throws Exception {
		JFileChooser chooser = makeJFileChooserForAppearanceExport();
		int returnVal = chooser.showSaveDialog(this);
		if (returnVal != JFileChooser.APPROVE_OPTION) {
			return;
		}

		writeAppearanceFile(chooser);
	}

	private JFileChooser makeJFileChooserForAppearanceExport() {
		JFileChooser chooser = new JFileChooser();
		if (lastAppearanceFilePath != null) {
			chooser.setCurrentDirectory(lastAppearanceFilePath.getParentFile());
			chooser.setSelectedFile(lastAppearanceFilePath);
		}
		chooser.setFileFilter(new FileNameExtensionFilter("SR3 appearance files", "sr3_app"));
		return chooser;
	}

	private void writeAppearanceFile(JFileChooser chooser) throws Exception {
		String filename = chooser.getSelectedFile().getPath();
		if (!filename.endsWith(".sr3_app")) {
			filename += ".sr3_app";
		}
		AppearanceRecordIO.writePlayer(saveFile.getAppearance(), filename);
		lastAppearanceFilePath = chooser.getSelectedFile();
	}

	public void importAppearance() throws Exception {
		JFileChooser chooser = makeJFileChooserForAppearanceImport();
		int returnVal = chooser.showOpenDialog(this);
		if (returnVal != JFileChooser.APPROVE_OPTION) {
			return;
		}

		File selectedFile = chooser.getSelectedFile();
		readAppearanceFile(selectedFile);
	}

	private JFileChooser makeJFileChooserForAppearanceImport() {
		JFileChooser chooser = new JFileChooser();
		if (lastAppearanceFilePath != null) {
			chooser.setCurrentDirectory(lastAppearanceFilePath.getParentFile());
		}
		chooser.setFileFilter(new FileNameExtensionFilter("SR3 appearance files", "sr3_app"));
		return chooser;
	}

	private void readAppearanceFile(File selectedFile) throws Exception {
		AppearanceRecord record = AppearanceRecordIO.readPlayer(selectedFile);
		saveFile.setAppearance(record);
		lastAppearanceFilePath = selectedFile;
	}

	private void setUiEnabledAfterLoading(boolean enabled) {
		saveButton.setEnabled(enabled);
		importAppearanceButton.setEnabled(enabled);
		exportAppearanceButton.setEnabled(enabled);
		garageButton.setEnabled(enabled);
		newGamePlusButton.setEnabled(enabled);
		setAllComponentsEnabled(tabbedPane, enabled);
	}

	private void validateInput() throws Exception {
	}

	@SuppressWarnings("FeatureEnvy")
	private void setPlayerPanelData() {
		saveFile.setMoney(((BigDecimal) moneyField.getValue()).multiply(ONE_HUNDRED).toBigInteger().intValue());
		saveFile.setRespectLevel(((Number) respectLevelField.getValue()).byteValue());
		saveFile.setLuchadoresNotorietyDecayRateMultiplier((Float) luchadoresNotorietyDecayRateMultiplierField.getValue());
		saveFile.setDeckersNotorietyDecayRateMultiplier((Float) deckersNotorietyDecayRateMultiplierField.getValue());
		saveFile.setMorningstarNotorietyDecayRateMultiplier((Float) morningstarNotorietyDecayRateMultiplierField.getValue());
		saveFile.setPoliceNotorietyDecayRateMultiplier((Float) policeNotorietyDecayRateMultiplierField.getValue());
		saveFile.setBulletDamageTakenMultiplier((Float) bulletDamageTakenMultiplierField.getValue());
		saveFile.setExplosiveDamageTakenMultiplier((Float) explosiveDamageTakenMultiplierField.getValue());
		saveFile.setVehicleDamageTakenMultiplier((Float) vehicleDamageTakenMultiplierField.getValue());
		saveFile.setFireDamageTakenMultiplier((Float) fireDamageTakenMultiplierField.getValue());
		saveFile.setFallingDamageTakenMultiplier((Float) fallingDamageTakenMultiplierField.getValue());
		saveFile.setHealthRegenMultiplier((Float) healthRegenMultiplierField.getValue());
		saveFile.setRespectMultiplier((Float) respectMultiplierField.getValue());
//		saveFile.setMeleeDamageMultiplier((Float) meleeDamageMultiplierField.getValue());
	}

	@SuppressWarnings("FeatureEnvy")
	private void readPlayerPanelData() {
		moneyField.setValue(new BigDecimal(saveFile.getMoney()).divide(ONE_HUNDRED));
		respectLevelField.setValue(saveFile.getRespectLevel());
		luchadoresNotorietyDecayRateMultiplierField.setValue(saveFile.getLuchadoresNotorietyDecayRateMultiplier());
		deckersNotorietyDecayRateMultiplierField.setValue(saveFile.getDeckersNotorietyDecayRateMultiplier());
		morningstarNotorietyDecayRateMultiplierField.setValue(saveFile.getMorningstarNotorietyDecayRateMultiplier());
		policeNotorietyDecayRateMultiplierField.setValue(saveFile.getPoliceNotorietyDecayRateMultiplier());
		bulletDamageTakenMultiplierField.setValue(saveFile.getBulletDamageTakenMultiplier());
		explosiveDamageTakenMultiplierField.setValue(saveFile.getExplosiveDamageTakenMultiplier());
		vehicleDamageTakenMultiplierField.setValue(saveFile.getVehicleDamageTakenMultiplier());
		fireDamageTakenMultiplierField.setValue(saveFile.getFireDamageTakenMultiplier());
		fallingDamageTakenMultiplierField.setValue(saveFile.getFallingDamageTakenMultiplier());
		healthRegenMultiplierField.setValue(saveFile.getHealthRegenMultiplier());
		respectMultiplierField.setValue(saveFile.getRespectMultiplier());
//		meleeDamageMultiplierField.setValue(saveFile.getMeleeDamageMultiplier());
	}

	private void writeWeaponUnlockStates() throws Exception {
		boolean[] weaponUnlocks = new boolean[Sr3SaveFile.NUMBER_OF_WEAPON_UNLOCKS];
		for (int i = 0; i < weaponUnlockCheckBoxes.size(); i++) {
			JCheckBox jCheckBox = weaponUnlockCheckBoxes.get(i);
			weaponUnlocks[i] = jCheckBox.isSelected();
		}
		saveFile.setWeaponUnlocks(weaponUnlocks);
	}

	private void setWeaponUnlockCheckBoxStates() throws Exception {
		boolean[] weaponUnlocks = saveFile.getWeaponUnlocks();
		for (int i = 0; i < weaponUnlockCheckBoxes.size(); i++) {
			JCheckBox jCheckBox = weaponUnlockCheckBoxes.get(i);
			jCheckBox.setSelected(weaponUnlocks[i]);
		}
	}

	private void writeWeaponUpgradeLevels() throws Exception {
		try {
			int[] weaponUpgradeHashes = new int[Sr3SaveFile.NUMBER_OF_WEAPON_UPGRADES];
			int[] weaponUpgradeLevels = new int[Sr3SaveFile.NUMBER_OF_WEAPON_UPGRADES];
			int upgradeIndex = 0;
			for (int i = 0; i < weaponUpgradeLevelSpinners.size(); i++) {
				JSpinner jSpinner = weaponUpgradeLevelSpinners.get(i);
				Integer value = (Integer) jSpinner.getValue();
				if (value > 1) {  // we skip the level 1 weapons
					weaponUpgradeHashes[upgradeIndex] = StringHashes.hash(weaponUnlockCheckBoxes.get(i).getText());
					weaponUpgradeLevels[upgradeIndex] = value;
					upgradeIndex++;
				}
			}
			saveFile.setWeaponUpgradeHashes(weaponUpgradeHashes);
			saveFile.setWeaponUpgradeLevels(weaponUpgradeLevels);
		} catch (ArrayIndexOutOfBoundsException ex) {
			throw new Exception("Only " + Sr3SaveFile.NUMBER_OF_WEAPON_UPGRADES + " weapons may be upgraded past level 1.", ex);
		}
	}

	private Map<Integer, Integer> getWeaponUpgradeLevels() throws Exception {
		int[] hashes = saveFile.getWeaponUpgradeHashes();
		int[] levels = saveFile.getWeaponUpgradeLevels();
		Map<Integer, Integer> weaponUpgrades = new HashMap<>(hashes.length);
		for (int i = 0; i < hashes.length; i++) {
			weaponUpgrades.put(hashes[i], levels[i]);
		}
		return weaponUpgrades;
	}

	private void writeUpgradeUnlockStates() throws Exception {
		boolean[] upgradeUnlocks = new boolean[Sr3SaveFile.NUMBER_OF_UPGRADE_UNLOCKS];
		for (int i = 0; i < upgradeUnlockCheckBoxes.size(); i++) {
			JCheckBox jCheckBox = upgradeUnlockCheckBoxes.get(i);
			upgradeUnlocks[i] = jCheckBox.isSelected();
		}
		saveFile.setUpgradeUnlocks(upgradeUnlocks);
	}

	private void setUpgradeUnlockCheckBoxStates() throws Exception {
		boolean[] upgradeUnlocks = saveFile.getUpgradeUnlocks();
		for (int i = 0; i < upgradeUnlockCheckBoxes.size(); i++) {
			JCheckBox jCheckBox = upgradeUnlockCheckBoxes.get(i);
			jCheckBox.setSelected(upgradeUnlocks[i]);
		}
	}

	private void writeUpgradeAvailabilityStates() throws Exception {
		boolean[] upgradeAvailability = new boolean[Sr3SaveFile.NUMBER_OF_UPGRADE_UNLOCKS];
		for (int i = 0; i < upgradeAvailabilityCheckBoxes.size(); i++) {
			JCheckBox jCheckBox = upgradeAvailabilityCheckBoxes.get(i);
			upgradeAvailability[i] = jCheckBox.isSelected();
		}
		saveFile.setUpgradeAvailability(upgradeAvailability);
	}

	private void setUpgradeAvailabilityCheckBoxStates() throws Exception {
		boolean[] upgradeAvailability = saveFile.getUpgradeAvailability();
		for (int i = 0; i < upgradeAvailabilityCheckBoxes.size(); i++) {
			JCheckBox jCheckBox = upgradeAvailabilityCheckBoxes.get(i);
			jCheckBox.setSelected(upgradeAvailability[i]);
		}
	}

	private void writeHomiesUnlockStates() throws Exception {
		int[] homiesStatuses = new int[Sr3SaveFile.NUMBER_OF_HOMIES];
		for (int i = 0; i < homiesUnlockCheckBoxes.size(); i++) {
			JCheckBox jCheckBox = homiesUnlockCheckBoxes.get(i);
			homiesStatuses[i] = jCheckBox.isSelected() ? 1 : 0;
		}
		saveFile.setHomieStatuses(homiesStatuses);
	}

	private void setHomiesUnlockCheckBoxStates() throws Exception {
		int[] homiesStatuses = saveFile.getHomieStatuses();
		for (int i = 0; i < homiesUnlockCheckBoxes.size(); i++) {
			JCheckBox jCheckBox = homiesUnlockCheckBoxes.get(i);
			jCheckBox.setSelected(homiesStatuses[i] > 0);
		}
	}

	private void writeDlcHomiesUnlockStates() throws Exception {
		int[] dlcHomiesStatuses = new int[Sr3SaveFile.NUMBER_OF_DLC_HOMIES];
		for (int i = 0; i < dlcHomiesUnlockCheckBoxes.size(); i++) {
			JCheckBox jCheckBox = dlcHomiesUnlockCheckBoxes.get(i);
			dlcHomiesStatuses[i] = jCheckBox.isSelected() ? 1 : 0;
		}
		saveFile.setDlcHomieStatuses(dlcHomiesStatuses);
	}

	private void setDlcHomiesUnlockCheckBoxStates() throws Exception {
		int[] dlcHomiesStatuses = saveFile.getDlcHomieStatuses();
		for (int i = 0; i < dlcHomiesUnlockCheckBoxes.size(); i++) {
			JCheckBox jCheckBox = dlcHomiesUnlockCheckBoxes.get(i);
			jCheckBox.setSelected(dlcHomiesStatuses[i] > 0);
		}
	}

	private void writeTerritoryStates() throws Exception {
		boolean[] territoryStates = new boolean[Sr3SaveFile.NUMBER_OF_TERRITORIES];
		for (int i = 0; i < territoriesOwnedCheckBoxes.size(); i++) {
			JCheckBox jCheckBox = territoriesOwnedCheckBoxes.get(i);
			territoryStates[i] = jCheckBox.isSelected();
		}
		saveFile.setTerritoryStatuses(territoryStates);
	}

	private void fixMissions() throws Exception {
		long[] missionStatus = saveFile.getMissionStatuses();
		for (int i = 0; i < missionStatus.length; i++) {
			if (missionStatus[i] > 0L) { // if the mission is complete
				missionStatus[i] |= 1L; // set the available flag on
			}
		}
		saveFile.setMissionStatuses(missionStatus);
	}

	private static class CustomScrollPane extends JScrollPane {

		private static final int SCROLL_UNIT_INCREMENT = 23;

		CustomScrollPane(Component view) {
			super(view);
			setProperties();
		}

		private void setProperties() {
			setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
			getVerticalScrollBar().setUnitIncrement(SCROLL_UNIT_INCREMENT);
		}
	}

	private class SaveButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				validateInput();
				setPlayerPanelData();
				writeWeaponUnlockStates();
				writeWeaponUpgradeLevels();
				writeUpgradeUnlockStates();
				writeUpgradeAvailabilityStates();
				writeHomiesUnlockStates();
				writeDlcHomiesUnlockStates();
				writeTerritoryStates();
				fixMissions();
				saveFile();
			} catch (Exception ex) {
				Logger.getLogger(GarageEditor.class.getName()).log(Level.SEVERE, null, ex);
				JOptionPane.showMessageDialog(MainFrame.this, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private class LoadButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				Sr3SaveFile newFile = loadSaveFile();
				if (newFile != null) {
					saveFile = newFile;
					readPlayerPanelData();
					readWeaponUpgrades();
					readUpgrades();
					readHomies();
					readTerritories();
					setUiEnabledAfterLoading(true);
				}
			} catch (Exception ex) {
				Logger.getLogger(GarageEditor.class.getName()).log(Level.SEVERE, null, ex);
				JOptionPane.showMessageDialog(MainFrame.this, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private class ImportAppearanceButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				importAppearance();
			} catch (Exception ex) {
				Logger.getLogger(GarageEditor.class.getName()).log(Level.SEVERE, null, ex);
				JOptionPane.showMessageDialog(MainFrame.this, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private class ExportAppearanceButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				exportAppearance();
			} catch (Exception ex) {
				Logger.getLogger(GarageEditor.class.getName()).log(Level.SEVERE, null, ex);
				JOptionPane.showMessageDialog(MainFrame.this, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private class GarageButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				GarageEditor editor = new GarageEditor(saveFile);
				editor.displayFrame(MainFrame.this);
			} catch (Exception ex) {
				Logger.getLogger(GarageEditor.class.getName()).log(Level.SEVERE, null, ex);
				JOptionPane.showMessageDialog(MainFrame.this, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private class NewGamePlusButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			int showOptionDialog = JOptionPane.showOptionDialog(MainFrame.this,
					"This will reset missions, activitites, territories, gang operations, properties, stores, and cribs.\n"
							+ "When you load the game, it will start the first mission automatically like with a new game.\n"
							+ "You will retain all of your upgrades, collectibles, money, respect, and garage vehicles.\n"
							+ '\n'
							+ "Continue?",
					"New Game +", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
			if (showOptionDialog == JOptionPane.NO_OPTION) {
				return;
			}
			try {
				saveFile.resetDataForNewGamePlus();
				readTerritories();
				// also remove unlimited ammo upgrade and level 4 SMG if they cause problems. doesn't look like they actually do. i'm leaving those out, for now.
				removeCribUpgrades();
				JOptionPane.showMessageDialog(MainFrame.this, "New Game + mode has been activated. Don't forget to save the file.", "Done", JOptionPane.INFORMATION_MESSAGE);
			} catch (Exception ex) {
				Logger.getLogger(GarageEditor.class.getName()).log(Level.SEVERE, null, ex);
				JOptionPane.showMessageDialog(MainFrame.this, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}
