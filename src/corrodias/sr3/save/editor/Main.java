package corrodias.sr3.save.editor;

import com.stackoverflow.question.number_62289.WinRegistry;
import corrodias.sr3.garage.manager.VehicleInfoParser;
import corrodias.sr3.save.data.ListReader;
import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;

public class Main {

	public static final int MAJOR_VERSION = 2;
	public static final int MINOR_VERSION = 3;

	@SuppressWarnings("MethodNamesDifferingOnlyByCase")
	public static void main(String[] args) {
		setLookAndFeel();
		ToolTipManager.sharedInstance().setDismissDelay(Integer.MAX_VALUE);

		try {
			if ((args.length > 1) && "outputnames".equals(args[0])) {
				new VehicleInfoParser(args[1]).outputVehicleInfo();
			} else {
				ListReader manager = new ListReader();
				JFrame mainFrame = new MainFrame(manager);
				mainFrame.setVisible(true);
			}
		} catch (Throwable ex) {
			Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public static void setLookAndFeel() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception ignored) {
		}
	}

	public static File getSavePath() {
		try {
			String steamPath = WinRegistry.readString(
					WinRegistry.HKEY_CURRENT_USER,
					"Software\\Valve\\Steam",
					"SteamPath");

			List<String> keys = WinRegistry.readStringSubKeys(
					WinRegistry.HKEY_CURRENT_USER,
					"Software\\Valve\\Steam\\Users");
			if (!keys.isEmpty()) {
				String userId = keys.get(0);
				String savePath = steamPath + "/userdata/" + userId + "/55230/remote/sr3save_00.sr3s_pc";
				return new File(savePath);
			}
		} catch (Exception ex) {
			// this function may fail at any point, because it's using an ugly reflection hack
			Logger.getLogger(Main.class.getName()).log(Level.INFO, ex.toString());
		}
		return null;
	}

	private Main() {
	}
}
