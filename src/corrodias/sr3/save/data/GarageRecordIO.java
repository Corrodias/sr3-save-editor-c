package corrodias.sr3.save.data;

import java.io.*;

public class GarageRecordIO {

	public static void writeVehicle(GarageRecord record, String filename) throws Exception {
		writeVehicle(record, new File(filename));
	}

	public static void writeVehicle(GarageRecord record, File file) throws Exception {
		try (BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(file))) {
			byte[] bytes = record.getBytes();
			output.write(bytes, 0, bytes.length);
		}
	}

	public static GarageRecord readVehicle(String filename) throws Exception {
		return readVehicle(new File(filename));
	}

	public static GarageRecord readVehicle(File file) throws Exception {
		GarageRecord record;
		try (BufferedInputStream input = new BufferedInputStream(new FileInputStream(file))) {
			byte[] rawRecord = new byte[GarageRecord.RECORD_SIZE];
			input.read(rawRecord);
			record = GarageRecord.parseBytes(rawRecord);
		}
		return record;
	}

	private GarageRecordIO() {
	}
}
