package corrodias.sr3.save.data;

/**
 * I'm not sure what the difference between level and level2 is. But they look like they are supposed to match.
 *
 * @author Corrodias
 */
public class CribRecord {

	public long hash;
	public boolean unowned;
	public int level;
	public int level2;
}
