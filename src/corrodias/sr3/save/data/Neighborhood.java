package corrodias.sr3.save.data;

public enum Neighborhood {

	Henry_Steel_Mills,
	Loren_Square,
	Sunset_Park,
	Wesley_Cutter_International_Airport,
	Camano_Place,
	Rosen_Oaks,
	Espina,
	Unknown,
	Burns_Hill,
	Ashwood,
	Salander,
	Brickston,
	Arapice_Island,
	The_Grove,
	Bridgeport,
	New_Baranec,
	Yearwood,
	Port_Pryor;

	@Override
	public String toString() {
		return super.toString().replace('_', ' ');
	}
}
