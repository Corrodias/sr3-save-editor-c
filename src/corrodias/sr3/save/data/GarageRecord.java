package corrodias.sr3.save.data;

import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class GarageRecord {

	public int index; // 0
	public byte[] other_1; // 4
	public short vehicle_id; // 12
	public byte[] other_2; // 14
	public String name;
	public String display_name;
	public VehicleType type;
	public static final int RECORD_SIZE = 112;

	private GarageRecord() {
	}

	public static GarageRecord parseBytes(byte[] record) {
		GarageRecord output = new GarageRecord();
		output.index = Array.getInt(record, 0);
		output.other_1 = Arrays.copyOfRange(record, 4, 4 + 8);
		output.vehicle_id = Array.getShort(record, 12);
		output.other_2 = Arrays.copyOfRange(record, 14, 112);
		output.display_name = Integer.toString(output.vehicle_id);
		return output;
	}

	public byte[] getBytes() {
		ByteBuffer output = ByteBuffer.allocate(RECORD_SIZE);
		output.order(ByteOrder.LITTLE_ENDIAN);
		output.putInt(index);
		output.put(other_1);
		output.putShort(vehicle_id);
		output.put(other_2);
		return output.array();
	}

	@Override
	public String toString() {
		return display_name;
	}
}
