package corrodias.sr3.save.data;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

public class Sr3SaveFile {

	public static final int GARAGE_SLOTS = 99; // determined experimentally. i'm not absolutely sure that this is the limit. there's room in the save file for 150, but far fewer show up in the game.
	public static final int TOTAL_FILE_SIZE = 108776;
	public static final int NUMBER_OF_WEAPON_UNLOCKS = 96;
	public static final int NUMBER_OF_UPGRADES = 350;
	public static final int NUMBER_OF_UPGRADE_UNLOCKS = 352;
	public static final int NUMBER_OF_TERRITORIES = 154;
	public static final int NUMBER_OF_WEAPON_UPGRADES = 50;
	public static final int NUMBER_OF_HOMIES = 28;
	public static final int NUMBER_OF_DLC_HOMIES = 16;
	public static final int NUMBER_OF_MISSIONS = 128;
	public static final int NUMBER_OF_ACTIVITITES = 60;
	public static final int NUMBER_OF_GANG_OPERATIONS = 32;
	public static final int NUMBER_OF_CRIBS = 10;
	public static final int NUMBER_OF_CHUNKS = 64; // don't rely on this. at the end of the game, it's like 13 long.
	//
	public ArrayList<GarageRecord> garageRecords; // this doesn't follow the paradigm of get/set used for the other things. it was made earlier and hasn't been refactored.
	//
	private static final int OFFSET_FOR_APPEARANCE = 2304;
	private static final int OFFSET_FOR_ACTIVITY_COMPLETION = 11156;
	private static final int OFFSET_FOR_ACTIVITY_DISCOVERY = 11928;
	private static final int OFFSET_FOR_GANG_OPERATION_DISCOVERY = 12856;
	private static final int OFFSET_FOR_GANG_OPERATION_COMPLETION = 13112;
	private static final int OFFSET_FOR_MISSION_COMPLETION = 14320;
	private static final int OFFSET_FOR_HOMIES = 17204;
	private static final int OFFSET_FOR_RESPECT_LEVEL = 17440;
	private static final int OFFSET_FOR_UGPRADES_LIST = 17448;
	private static final int OFFSET_FOR_UPGRADES_UNLOCKED = 18848;
	private static final int OFFSET_FOR_UPGRADES_AVAILABLE = 18892;
	private static final int OFFSET_FOR_UPGRADES_NEW = 18936;
	private static final int OFFSET_FOR_LUCHADORES_NOTORIETY_DECAY_RATE_MULTIPLIER = 18980;
	private static final int OFFSET_FOR_DECKERS_NOTORIETY_DECAY_RATE_MULTIPLIER = 18984;
	private static final int OFFSET_FOR_MORNINGSTAR_NOTORIETY_DECAY_RATE_MULTIPLIER = 18988;
	private static final int OFFSET_FOR_POLICE_NOTORIETY_DECAY_RATE_MULTIPLIER = 18992;
	private static final int OFFSET_FOR_BULLET_DAMAGE_TAKEN_MULTIPLIER = 18996;
	private static final int OFFSET_FOR_EXPLOSIVE_DAMAGE_TAKEN_MULTIPLIER = 19000;
	private static final int OFFSET_FOR_VEHICLE_DAMAGE_TAKEN_MULTIPLIER = 19004;
	private static final int OFFSET_FOR_FIRE_DAMAGE_TAKEN_MULTIPLIER = 19008;
	private static final int OFFSET_FOR_FALLING_DAMAGE_TAKEN_MULTIPLIER = 19012;
	private static final int OFFSET_FOR_HEALTH_REGEN_MULTIPLIER = 19020;
	private static final int OFFSET_FOR_RESPECT_MULTIPLIER = 19036;
	private static final int OFFSET_FOR_MELEE_DAMAGE_MULTIPLIER = 19040;
	private static final int OFFSET_FOR_BONUS_HOURLY_CITY_INCOME = 19054;
	private static final int OFFSET_FOR_MONEY = 19056;
	private static final int OFFSET_FOR_WEAPON_SLOTS = 19064;
	private static final int OFFSET_FOR_WEAPON_UPGRADES = 19400;
	private static final int OFFSET_FOR_WEAPON_UPGRADE_LEVELS = 19600;
	private static final int OFFSET_FOR_CRIBS = 20072;
	private static final int OFFSET_FOR_WEAPON_UNLOCKS = 20316;
	private static final int OFFSET_FOR_GARAGE = 46944;
	private static final int OFFSET_FOR_CHUNKS = 63976;
	private static final int OFFSET_FOR_PROPERTY_OWNERSHIP = 69020;
	private static final int OFFSET_FOR_PROPERTY_DISCOVERY = 69029;
	private static final int OFFSET_FOR_TERRITORIES = 70592;
	private static final int OFFSET_FOR_DLC_HOMIES = 91796;
	//
	private static final int SIZE_OF_APPEARANCE = 8788;
	private static final long DEFAULT_ACTIVITY_DISCOVERY = 350680826864434599L;
	private static final long MISSION_1_5_HASH = 70791540806071269L;
	//
	private byte[] bytes;
	private ByteBuffer buffer;

	public Sr3SaveFile() {
	}

	public static Sr3SaveFile parseBytes(byte[] saveFile) {
		ByteBuffer input = ByteBuffer.wrap(saveFile);
		input.order(ByteOrder.LITTLE_ENDIAN);

		Sr3SaveFile output = new Sr3SaveFile();

		output.bytes = new byte[TOTAL_FILE_SIZE];
		input.get(output.bytes);

		input.position(OFFSET_FOR_GARAGE);
		int numberOfOccupiedGarageRecords = input.getInt();

		output.garageRecords = new ArrayList<>(GARAGE_SLOTS);
		for (int i = 0; i < GARAGE_SLOTS; i++) {
			byte[] rawRecord = new byte[GarageRecord.RECORD_SIZE];
			input.get(rawRecord);
			if (i < numberOfOccupiedGarageRecords) {
				GarageRecord record = GarageRecord.parseBytes(rawRecord);
				output.garageRecords.add(record);
			}
		}

		output.buffer = ByteBuffer.wrap(output.bytes);
		output.buffer.order(ByteOrder.LITTLE_ENDIAN);

		return output;
	}

	public byte[] getBytes() throws Exception {
		ByteBuffer output = ByteBuffer.allocate(TOTAL_FILE_SIZE);
		output.order(ByteOrder.LITTLE_ENDIAN);
		output.put(bytes);

		output.position(OFFSET_FOR_GARAGE + 4);
		byte[] emptyGarageSpace = new byte[GARAGE_SLOTS * GarageRecord.RECORD_SIZE];
		output.put(emptyGarageSpace);

		output.position(OFFSET_FOR_GARAGE);
		output.putInt(garageRecords.size());
		for (int i = 0; i < garageRecords.size(); i++) {
			byte[] rawRecord = garageRecords.get(i).getBytes();
			output.put(rawRecord);
		}
		return output.array();
	}

	private void copyByteArrayIntoBooleanArrayInReverseBitOrder(byte[] bitArray, boolean[] booleans) {
		for (int i = 0; i < bitArray.length; i++) {
			booleans[(i * 8) + 0] = (bitArray[i] & 0b00000001) > 0;
			booleans[(i * 8) + 1] = (bitArray[i] & 0b00000010) > 0;
			booleans[(i * 8) + 2] = (bitArray[i] & 0b00000100) > 0;
			booleans[(i * 8) + 3] = (bitArray[i] & 0b00001000) > 0;
			booleans[(i * 8) + 4] = (bitArray[i] & 0b00010000) > 0;
			booleans[(i * 8) + 5] = (bitArray[i] & 0b00100000) > 0;
			booleans[(i * 8) + 6] = (bitArray[i] & 0b01000000) > 0;
			booleans[(i * 8) + 7] = (bitArray[i] & 0b10000000) > 0;
		}
	}

	private void copyBooleanArrayIntoByteArrayInReverseBitOrder(boolean[] booleans, byte[] bitArray) {
		for (int i = 0; i < bitArray.length; i++) {
			bitArray[i] |= booleans[(i * 8) + 0] ? 0b00000001 : 0;
			bitArray[i] |= booleans[(i * 8) + 1] ? 0b00000010 : 0;
			bitArray[i] |= booleans[(i * 8) + 2] ? 0b00000100 : 0;
			bitArray[i] |= booleans[(i * 8) + 3] ? 0b00001000 : 0;
			bitArray[i] |= booleans[(i * 8) + 4] ? 0b00010000 : 0;
			bitArray[i] |= booleans[(i * 8) + 5] ? 0b00100000 : 0;
			bitArray[i] |= booleans[(i * 8) + 6] ? 0b01000000 : 0;
			bitArray[i] |= booleans[(i * 8) + 7] ? 0b10000000 : 0;
		}
	}

	public void setMoney(int money) {
		buffer.position(OFFSET_FOR_MONEY);
		buffer.putInt(money);
	}

	public int getMoney() {
		buffer.position(OFFSET_FOR_MONEY);
		return buffer.getInt();
	}

	public void setLuchadoresNotorietyDecayRateMultiplier(float mult) {
		buffer.position(OFFSET_FOR_LUCHADORES_NOTORIETY_DECAY_RATE_MULTIPLIER);
		buffer.putFloat(mult);
	}

	public float getLuchadoresNotorietyDecayRateMultiplier() {
		buffer.position(OFFSET_FOR_LUCHADORES_NOTORIETY_DECAY_RATE_MULTIPLIER);
		return buffer.getFloat();
	}

	public void setDeckersNotorietyDecayRateMultiplier(float mult) {
		buffer.position(OFFSET_FOR_DECKERS_NOTORIETY_DECAY_RATE_MULTIPLIER);
		buffer.putFloat(mult);
	}

	public float getDeckersNotorietyDecayRateMultiplier() {
		buffer.position(OFFSET_FOR_DECKERS_NOTORIETY_DECAY_RATE_MULTIPLIER);
		return buffer.getFloat();
	}

	public void setMorningstarNotorietyDecayRateMultiplier(float mult) {
		buffer.position(OFFSET_FOR_MORNINGSTAR_NOTORIETY_DECAY_RATE_MULTIPLIER);
		buffer.putFloat(mult);
	}

	public float getMorningstarNotorietyDecayRateMultiplier() {
		buffer.position(OFFSET_FOR_MORNINGSTAR_NOTORIETY_DECAY_RATE_MULTIPLIER);
		return buffer.getFloat();
	}

	public void setPoliceNotorietyDecayRateMultiplier(float mult) {
		buffer.position(OFFSET_FOR_POLICE_NOTORIETY_DECAY_RATE_MULTIPLIER);
		buffer.putFloat(mult);
	}

	public float getPoliceNotorietyDecayRateMultiplier() {
		buffer.position(OFFSET_FOR_POLICE_NOTORIETY_DECAY_RATE_MULTIPLIER);
		return buffer.getFloat();
	}

	public void setBulletDamageTakenMultiplier(float mult) {
		buffer.position(OFFSET_FOR_BULLET_DAMAGE_TAKEN_MULTIPLIER);
		buffer.putFloat(mult);
	}

	public float getBulletDamageTakenMultiplier() {
		buffer.position(OFFSET_FOR_BULLET_DAMAGE_TAKEN_MULTIPLIER);
		return buffer.getFloat();
	}

	public void setExplosiveDamageTakenMultiplier(float mult) {
		buffer.position(OFFSET_FOR_EXPLOSIVE_DAMAGE_TAKEN_MULTIPLIER);
		buffer.putFloat(mult);
	}

	public float getExplosiveDamageTakenMultiplier() {
		buffer.position(OFFSET_FOR_EXPLOSIVE_DAMAGE_TAKEN_MULTIPLIER);
		return buffer.getFloat();
	}

	public void setVehicleDamageTakenMultiplier(float mult) {
		buffer.position(OFFSET_FOR_VEHICLE_DAMAGE_TAKEN_MULTIPLIER);
		buffer.putFloat(mult);
	}

	public float getVehicleDamageTakenMultiplier() {
		buffer.position(OFFSET_FOR_VEHICLE_DAMAGE_TAKEN_MULTIPLIER);
		return buffer.getFloat();
	}

	public void setFireDamageTakenMultiplier(float mult) {
		buffer.position(OFFSET_FOR_FIRE_DAMAGE_TAKEN_MULTIPLIER);
		buffer.putFloat(mult);
	}

	public float getFireDamageTakenMultiplier() {
		buffer.position(OFFSET_FOR_FIRE_DAMAGE_TAKEN_MULTIPLIER);
		return buffer.getFloat();
	}

	public void setFallingDamageTakenMultiplier(float mult) {
		buffer.position(OFFSET_FOR_FALLING_DAMAGE_TAKEN_MULTIPLIER);
		buffer.putFloat(mult);
	}

	public float getFallingDamageTakenMultiplier() {
		buffer.position(OFFSET_FOR_FALLING_DAMAGE_TAKEN_MULTIPLIER);
		return buffer.getFloat();
	}

	public void setHealthRegenMultiplier(float mult) {
		buffer.position(OFFSET_FOR_HEALTH_REGEN_MULTIPLIER);
		buffer.putFloat(mult);
	}

	public float getHealthRegenMultiplier() {
		buffer.position(OFFSET_FOR_HEALTH_REGEN_MULTIPLIER);
		return buffer.getFloat();
	}

	public void setRespectMultiplier(float mult) {
		buffer.position(OFFSET_FOR_RESPECT_MULTIPLIER);
		buffer.putFloat(mult);
	}

	public float getRespectMultiplier() {
		buffer.position(OFFSET_FOR_RESPECT_MULTIPLIER);
		return buffer.getFloat();
	}

	public void setMeleeDamageMultiplier(float mult) {
		buffer.position(OFFSET_FOR_MELEE_DAMAGE_MULTIPLIER);
		buffer.putFloat(mult);
	}

	public float getMeleeDamageMultiplier() {
		buffer.position(OFFSET_FOR_MELEE_DAMAGE_MULTIPLIER);
		return buffer.getFloat();
	}

	public void setWeaponUnlocks(boolean[] weaponUnlocks) {
		byte[] temp = new byte[NUMBER_OF_WEAPON_UNLOCKS / 8]; // 12
		copyBooleanArrayIntoByteArrayInReverseBitOrder(weaponUnlocks, temp);
		buffer.position(OFFSET_FOR_WEAPON_UNLOCKS);
		buffer.put(temp);
	}

	public boolean[] getWeaponUnlocks() {
		byte[] temp = new byte[NUMBER_OF_WEAPON_UNLOCKS / 8]; // 12
		buffer.position(OFFSET_FOR_WEAPON_UNLOCKS);
		buffer.get(temp);
		boolean[] weaponUnlocks = new boolean[NUMBER_OF_WEAPON_UNLOCKS];
		copyByteArrayIntoBooleanArrayInReverseBitOrder(temp, weaponUnlocks);
		return weaponUnlocks;
	}

	public void setWeaponUpgradeHashes(int[] weaponUpgradeHashes) {
		byte[] blank = new byte[4 * NUMBER_OF_WEAPON_UPGRADES];
		buffer.position(OFFSET_FOR_WEAPON_UPGRADES);
		buffer.put(blank);

		buffer.position(OFFSET_FOR_WEAPON_UPGRADES);
		for (int i = 0; i < NUMBER_OF_WEAPON_UPGRADES; i++) {
			buffer.putInt(weaponUpgradeHashes[i]);
		}
	}

	public int[] getWeaponUpgradeHashes() {
		int[] upgradeHashes = new int[NUMBER_OF_WEAPON_UPGRADES];
		buffer.position(OFFSET_FOR_WEAPON_UPGRADES);
		for (int i = 0; i < upgradeHashes.length; i++) {
			upgradeHashes[i] = buffer.getInt();
		}
		return upgradeHashes;
	}

	public void setWeaponUpgradeLevels(int[] weaponUpgradeLevels) {
		byte[] blank = new byte[4 * NUMBER_OF_WEAPON_UPGRADES];
		buffer.position(OFFSET_FOR_WEAPON_UPGRADE_LEVELS);
		buffer.put(blank);

		buffer.position(OFFSET_FOR_WEAPON_UPGRADE_LEVELS);
		for (int i = 0; i < NUMBER_OF_WEAPON_UPGRADES; i++) {
			int upgrades = weaponUpgradeLevels[i] - 1;
			int result = 0;
			for (int j = 0; j < upgrades; j++) {
				result <<= 1;
				result |= 1;
			}
			buffer.putInt(result);
		}
	}

	public int[] getWeaponUpgradeLevels() {
		int[] upgradeLevels = new int[NUMBER_OF_WEAPON_UPGRADES];
		buffer.position(OFFSET_FOR_WEAPON_UPGRADE_LEVELS);
		for (int i = 0; i < upgradeLevels.length; i++) {
			upgradeLevels[i] = Integer.bitCount(buffer.getInt()) + 1;
		}
		return upgradeLevels;
	}

	public void setUpgradeHashes(int[] upgradeHashes) {
		byte[] blank = new byte[4 * NUMBER_OF_UPGRADES];
		buffer.position(OFFSET_FOR_UGPRADES_LIST);
		buffer.put(blank);

		buffer.position(OFFSET_FOR_UGPRADES_LIST);
		for (int i = 0; i < NUMBER_OF_UPGRADES; i++) {
			buffer.putInt(upgradeHashes[i]);
		}
	}

	public int[] getUpgradeHashes() {
		int[] upgradeHashes = new int[NUMBER_OF_UPGRADES];
		buffer.position(OFFSET_FOR_UGPRADES_LIST);
		for (int i = 0; i < upgradeHashes.length; i++) {
			upgradeHashes[i] = buffer.getInt();
		}
		return upgradeHashes;
	}

	public void setUpgradeUnlocks(boolean[] upgradeUnlocks) {
		byte[] temp = new byte[NUMBER_OF_UPGRADE_UNLOCKS / 8]; // 350
		copyBooleanArrayIntoByteArrayInReverseBitOrder(upgradeUnlocks, temp);
		buffer.position(OFFSET_FOR_UPGRADES_UNLOCKED);
		buffer.put(temp);
	}

	public boolean[] getUpgradeUnlocks() {
		byte[] temp = new byte[NUMBER_OF_UPGRADE_UNLOCKS / 8]; // 350
		buffer.position(OFFSET_FOR_UPGRADES_UNLOCKED);
		buffer.get(temp);
		boolean[] upgradeUnlocks = new boolean[NUMBER_OF_UPGRADE_UNLOCKS];
		copyByteArrayIntoBooleanArrayInReverseBitOrder(temp, upgradeUnlocks);
		return upgradeUnlocks;
	}

	public void setUpgradeAvailability(boolean[] upgradeAvailability) {
		byte[] temp = new byte[NUMBER_OF_UPGRADE_UNLOCKS / 8]; // 350
		copyBooleanArrayIntoByteArrayInReverseBitOrder(upgradeAvailability, temp);
		buffer.position(OFFSET_FOR_UPGRADES_AVAILABLE);
		buffer.put(temp);
	}

	public boolean[] getUpgradeAvailability() {
		byte[] temp = new byte[NUMBER_OF_UPGRADE_UNLOCKS / 8]; // 350
		buffer.position(OFFSET_FOR_UPGRADES_AVAILABLE);
		buffer.get(temp);
		boolean[] upgradeUnlocks = new boolean[NUMBER_OF_UPGRADE_UNLOCKS];
		copyByteArrayIntoBooleanArrayInReverseBitOrder(temp, upgradeUnlocks);
		return upgradeUnlocks;
	}

	public void setHomieHashes(int[] homieHashes) {
		buffer.position(OFFSET_FOR_HOMIES);
		for (int i = 0; i < NUMBER_OF_HOMIES; i++) {
			buffer.putInt(homieHashes[i]);
			buffer.position(buffer.position() + 4); // skip the status
		}
	}

	public int[] getHomieHashes() {
		int[] homieHashes = new int[NUMBER_OF_HOMIES];
		buffer.position(OFFSET_FOR_HOMIES);
		for (int i = 0; i < homieHashes.length; i++) {
			homieHashes[i] = buffer.getInt();
			buffer.position(buffer.position() + 4); // skip the status
		}
		return homieHashes;
	}

	public void setHomieStatuses(int[] homieStatuses) {
		buffer.position(OFFSET_FOR_HOMIES + 4);
		for (int i = 0; i < NUMBER_OF_HOMIES; i++) {
			buffer.putInt(homieStatuses[i]);
			buffer.position(buffer.position() + 4); // skip the hash
		}
	}

	public int[] getHomieStatuses() {
		int[] homieStatuses = new int[NUMBER_OF_HOMIES];
		buffer.position(OFFSET_FOR_HOMIES + 4);
		for (int i = 0; i < homieStatuses.length; i++) {
			homieStatuses[i] = buffer.getInt();
			buffer.position(buffer.position() + 4); // skip the hash
		}
		return homieStatuses;
	}

	public void setDlcHomieHashes(int[] homieHashes) {
		buffer.position(OFFSET_FOR_DLC_HOMIES);
		for (int i = 0; i < NUMBER_OF_DLC_HOMIES; i++) {
			buffer.putInt(homieHashes[i]);
			buffer.position(buffer.position() + 4); // skip the status
		}
	}

	public int[] getDlcHomieHashes() {
		int[] homieHashes = new int[NUMBER_OF_DLC_HOMIES];
		buffer.position(OFFSET_FOR_DLC_HOMIES);
		for (int i = 0; i < homieHashes.length; i++) {
			homieHashes[i] = buffer.getInt();
			buffer.position(buffer.position() + 4); // skip the status
		}
		return homieHashes;
	}

	public void setDlcHomieStatuses(int[] homieStatuses) {
		buffer.position(OFFSET_FOR_DLC_HOMIES + 4);
		for (int i = 0; i < NUMBER_OF_DLC_HOMIES; i++) {
			buffer.putInt(homieStatuses[i]);
			buffer.position(buffer.position() + 4); // skip the hash
		}
	}

	public int[] getDlcHomieStatuses() {
		int[] homieStatuses = new int[NUMBER_OF_DLC_HOMIES];
		buffer.position(OFFSET_FOR_DLC_HOMIES + 4);
		for (int i = 0; i < homieStatuses.length; i++) {
			homieStatuses[i] = buffer.getInt();
			buffer.position(buffer.position() + 4); // skip the hash
		}
		return homieStatuses;
	}

	public void setRespectLevel(byte level) {
		bytes[OFFSET_FOR_RESPECT_LEVEL] = (byte) (level - 1);
	}

	public byte getRespectLevel() {
		return (byte) (bytes[OFFSET_FOR_RESPECT_LEVEL] + 1);
	}

	public void resetTerritories() {
		setTerritoryStatuses(new boolean[Sr3SaveFile.NUMBER_OF_TERRITORIES]);
	}

	public void setTerritoryStatuses(boolean[] owned) {
		for (int i = 0; i < NUMBER_OF_TERRITORIES; i++) {
			buffer.position(OFFSET_FOR_TERRITORIES + (i * 16) + 8);
			buffer.put((byte) (owned[i] ? 1 : 0));
		}
	}

	public boolean[] getTerritoryStatuses() {
		boolean[] territories = new boolean[NUMBER_OF_TERRITORIES];
		for (int i = 0; i < territories.length; i++) {
			buffer.position(OFFSET_FOR_TERRITORIES + (i * 16) + 8);
			territories[i] = buffer.get() == 1;
		}
		return territories;
	}

	public void resetActivities() {
		resetActivityCompletion();
		resetActivityDiscovery();
	}

	public void setActivityHashes(int[] activityHashes) {
		for (int i = 0; i < NUMBER_OF_ACTIVITITES; i++) {
			buffer.position(OFFSET_FOR_ACTIVITY_COMPLETION + (i * 12));
			buffer.putInt(activityHashes[i]);
		}
	}

	public int[] getActivityHashes() {
		int[] activityHashes = new int[NUMBER_OF_ACTIVITITES];
		for (int i = 0; i < activityHashes.length; i++) {
			buffer.position(OFFSET_FOR_ACTIVITY_COMPLETION + (i * 12));
			activityHashes[i] = buffer.getInt();
		}
		return activityHashes;
	}

	public void resetActivityCompletion() {
		for (int i = 0; i < NUMBER_OF_ACTIVITITES; i++) {
			buffer.position(OFFSET_FOR_ACTIVITY_COMPLETION + (i * 12) + 4); // skip the hash, but not the generic complete status
			buffer.putLong(0);
		}
	}

	public void setActivityCompletion(ActivityCompletion[] activityCompletion) {
		for (int i = 0; i < NUMBER_OF_ACTIVITITES; i++) {
			buffer.position(OFFSET_FOR_ACTIVITY_COMPLETION + (i * 12) + 4); // skip the hash, but not the generic complete status
			ActivityCompletion status = activityCompletion[i];
			buffer.putInt(status.singlePlayer || status.coop ? 1 : 0); // always 1 if complete, i guess
			buffer.put((byte) (status.singlePlayer ? 1 : 0)); // single player complete
			buffer.put((byte) (status.coop ? 1 : 0)); // co-op complete
			buffer.put((byte) 0); // filler
			buffer.put((byte) 0); // filler
		}
	}

	public ActivityCompletion[] getActivityCompletion() {
		ActivityCompletion[] activityCompletion = new ActivityCompletion[NUMBER_OF_ACTIVITITES];
		for (int i = 0; i < activityCompletion.length; i++) {
			buffer.position(OFFSET_FOR_ACTIVITY_COMPLETION + (i * 12) + 8); // skip the hash and the generic complete status
			int number = buffer.getInt();
			ActivityCompletion status = new ActivityCompletion();
			status.singlePlayer = (number & 0x0001) > 0;
			status.coop = (number & 0x0100) > 0;
		}
		return activityCompletion;
	}

	public void resetActivityDiscovery() {
		setActivityDiscovery(DEFAULT_ACTIVITY_DISCOVERY);
	}

	public void setActivityDiscovery(long value) {
		buffer.position(OFFSET_FOR_ACTIVITY_DISCOVERY);
		buffer.putLong(value);
	}

	public long getDefaultActivityDiscovery() {
		return DEFAULT_ACTIVITY_DISCOVERY; // i don't know exactly how this works, but this is what it's set to when you start a new game. most likely it's a bit field.
	}

	public long getActivityDiscovery() {
		buffer.position(OFFSET_FOR_ACTIVITY_DISCOVERY);
		return buffer.getLong();
	}

	public void resetMissions() {
		long[] hashes = getMissionHashes();
		long[] statuses = new long[Sr3SaveFile.NUMBER_OF_MISSIONS];
		for (int i = 0; i < hashes.length; i++) {
			long hash = hashes[i];
			if (hash == MISSION_1_5_HASH) {
				statuses[i] = 0xC5L;
			}
		}
		setMissionStatuses(statuses);
	}

	public void setMissionHashes(long[] missionHashes) {
		for (int i = 0; i < NUMBER_OF_MISSIONS; i++) {
			buffer.position(OFFSET_FOR_MISSION_COMPLETION + (i * 16));
			buffer.putLong(missionHashes[i]);
		}
	}

	public long[] getMissionHashes() {
		long[] homieHashes = new long[NUMBER_OF_MISSIONS];
		for (int i = 0; i < homieHashes.length; i++) {
			buffer.position(OFFSET_FOR_MISSION_COMPLETION + (i * 16));
			homieHashes[i] = buffer.getLong();
		}
		return homieHashes;
	}

	public void setMissionStatuses(long[] missionStatuses) {
		for (int i = 0; i < NUMBER_OF_MISSIONS; i++) {
			buffer.position(OFFSET_FOR_MISSION_COMPLETION + (i * 16) + 8); // skip the hash
			buffer.putLong(missionStatuses[i]);
		}
	}

	public long[] getMissionStatuses() {
		long[] homieHashes = new long[NUMBER_OF_MISSIONS];
		for (int i = 0; i < homieHashes.length; i++) {
			buffer.position(OFFSET_FOR_MISSION_COMPLETION + (i * 16) + 8); // skip the hash
			homieHashes[i] = buffer.getLong();
		}
		return homieHashes;
	}

	public static long getMission_1_5_Hash() {
		return MISSION_1_5_HASH;
	}

	public void setAppearance(AppearanceRecord appearance) {
		buffer.position(OFFSET_FOR_APPEARANCE);
		buffer.put(appearance.getBytes());
	}

	public AppearanceRecord getAppearance() {
		byte[] appearance = new byte[SIZE_OF_APPEARANCE];
		buffer.position(OFFSET_FOR_APPEARANCE);
		buffer.get(appearance);
		return AppearanceRecord.parseBytes(appearance);
	}

	public void resetGangOperations() {
		setGangOperationCompletion(new boolean[NUMBER_OF_GANG_OPERATIONS]);
		setGangOperationDiscovery(new long[NUMBER_OF_GANG_OPERATIONS]);
	}

	public void setGangOperationDiscovery(long[] operationHashes) {
		buffer.position(OFFSET_FOR_GANG_OPERATION_DISCOVERY);
		for (int i = 0; i < NUMBER_OF_GANG_OPERATIONS; i++) {
			buffer.putLong(operationHashes[i]);
		}
	}

	/**
	 * This is a list of long hashes which represent the discovered gang operations.
	 *
	 * @return
	 */
	public long[] getGangOperationDiscovery() {
		long[] operations = new long[NUMBER_OF_GANG_OPERATIONS];
		buffer.position(OFFSET_FOR_GANG_OPERATION_DISCOVERY);
		for (int i = 0; i < operations.length; i++) {
			operations[i] = buffer.getLong(); // they're sequential, so we don't need to move the buffer
		}
		return operations;
	}

	public void setGangOperationCompletion(boolean[] operationsComplete) {
		byte[] temp = new byte[NUMBER_OF_GANG_OPERATIONS / 8]; // 4
		copyBooleanArrayIntoByteArrayInReverseBitOrder(operationsComplete, temp);
		buffer.position(OFFSET_FOR_GANG_OPERATION_COMPLETION);
		buffer.put(temp);
	}

	/**
	 * This lines up with the gang operation discovery array.
	 *
	 * @return
	 */
	public boolean[] getGangOperationCompletion() {
		byte[] temp = new byte[NUMBER_OF_GANG_OPERATIONS / 8]; // 4
		buffer.position(OFFSET_FOR_GANG_OPERATION_COMPLETION);
		buffer.get(temp);
		boolean[] operationsComplete = new boolean[NUMBER_OF_GANG_OPERATIONS];
		copyByteArrayIntoBooleanArrayInReverseBitOrder(temp, operationsComplete);
		return operationsComplete;
	}

	public void resetProperties() {
		setPropertyDiscovery(0L);
		setPropertyOwnership(0L);
	}

	public void setPropertyDiscovery(long value) {
		buffer.position(OFFSET_FOR_PROPERTY_DISCOVERY);
		buffer.putLong(value);
	}

	public long getPropertyDiscovery() {
		buffer.position(OFFSET_FOR_PROPERTY_DISCOVERY);
		return buffer.getLong();
	}

	public void setPropertyOwnership(long value) {
		buffer.position(OFFSET_FOR_PROPERTY_OWNERSHIP);
		buffer.putLong(value);
	}

	public long getPropertyOwnership() {
		buffer.position(OFFSET_FOR_PROPERTY_OWNERSHIP);
		return buffer.getLong();
	}

	public void resetCribs() {
		CribRecord[] cribs = getCribs();
		for (CribRecord crib : cribs) {
			crib.unowned = true;
			crib.level = 0;
			crib.level2 = 0;
		}
		setCribs(cribs);
	}

	public void setCribs(CribRecord[] cribs) {
		buffer.position(OFFSET_FOR_CRIBS);
		for (int i = 0; i < NUMBER_OF_CRIBS; i++) {
			buffer.putLong(cribs[i].hash);
			buffer.putInt(cribs[i].unowned ? 1 : 0);
			buffer.putInt(cribs[i].level);
			buffer.putInt(cribs[i].level2);
			buffer.putInt(0);
		}
	}

	public CribRecord[] getCribs() {
		CribRecord[] cribs = new CribRecord[NUMBER_OF_CRIBS];
		for (int i = 0; i < cribs.length; i++) {
			buffer.position(OFFSET_FOR_CRIBS + (i * 24));
			cribs[i] = new CribRecord();
			cribs[i].hash = buffer.getLong();
			cribs[i].unowned = buffer.getInt() == 1;
			cribs[i].level = buffer.getInt();
			cribs[i].level2 = buffer.getInt();
			// plus one int of nothing, looks like
		}
		return cribs;
	}
	
	public void resetSwappedChunks() {
		setSwappedChunkHashes(new ArrayList<Integer>(0));
	}
	
	public void setSwappedChunkHashes(List<Integer> chunks) {
		buffer.position(OFFSET_FOR_CHUNKS);
		buffer.putInt(chunks.size());
		for (Integer hash : chunks) {
			buffer.putInt(hash);
		}
		int emptySpaces = NUMBER_OF_CHUNKS - chunks.size();
		for (int i = 0; i < emptySpaces; i++) {
			buffer.putInt(0);
		}
	}
	
	/**
	 * A chunk is a piece of the world that can be swapped in, like the saints cribs which replace the strongholds.
	 * The save file keeps a list of which ones have been swapped in.
	 * @return 
	 */
	public List<Integer> getSwappedChunkHashes() {
		buffer.position(OFFSET_FOR_CHUNKS);
		int length = buffer.getInt();
		List<Integer> chunks = new ArrayList<>(length);
		for (int i = 0; i < length; i++) {
			chunks.add(buffer.getInt());
		}
		return chunks;
	}

	public void resetDataForNewGamePlus() {
		resetMissions();
		resetActivities();
		resetTerritories();
		resetGangOperations();
		resetProperties();
		resetCribs();
		resetSwappedChunks();
	}
}
