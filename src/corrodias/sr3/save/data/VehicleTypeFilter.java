package corrodias.sr3.save.data;

import java.util.Arrays;
import java.util.List;

public enum VehicleTypeFilter {

	ALL("All", (VehicleType[]) null),
	AUTO("Automobile", VehicleType.AUTO),
	BIKE("Motorcycle", VehicleType.BIKE),
	AUTOANDBIKE("Auto & Bike", VehicleType.AUTO, VehicleType.BIKE),
	HELI("Helicopter", VehicleType.HELI),
	VTOL("VTOL", VehicleType.VTOL),
	HELIANDVTOL("Heli & VTOL", VehicleType.HELI, VehicleType.VTOL),
	BOAT("Watercraft", VehicleType.BOAT),
	PLANE("Airplane", VehicleType.PLANE);
	private String displayName;
	private List<VehicleType> acceptedTypes;

	private VehicleTypeFilter(String displayName, VehicleType... acceptedTypes) {
		this.displayName = displayName;
		if (acceptedTypes != null) {
			this.acceptedTypes = Arrays.asList(acceptedTypes);
		}
	}

	public boolean accept(VehicleType vehicleType) {
		return acceptedTypes == null ? true : acceptedTypes.contains(vehicleType);
	}

	@Override
	public String toString() {
		return displayName;
	}
}
