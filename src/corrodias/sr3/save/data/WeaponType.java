package corrodias.sr3.save.data;

public enum WeaponType {

	Melee, Pistol, SMG, Shotgun, Rifle, Explosive, Special, Grenade, Molotov, Flashbang, Electric_Grenade;
}
