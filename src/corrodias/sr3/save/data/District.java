package corrodias.sr3.save.data;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public enum District {

	Deckers("Stanfield (Deckers)", Arrays.asList(Neighborhood.Burns_Hill, Neighborhood.Ashwood, Neighborhood.Salander, Neighborhood.Brickston)),
	Luchadores("Carver Island (Luchadores)", Arrays.asList(Neighborhood.Arapice_Island, Neighborhood.The_Grove, Neighborhood.Bridgeport, Neighborhood.New_Baranec, Neighborhood.Yearwood, Neighborhood.Port_Pryor)),
	Morningstar_West("Downtown (Morningstar)", Arrays.asList(Neighborhood.Henry_Steel_Mills, Neighborhood.Loren_Square, Neighborhood.Sunset_Park)),
	Morningstar_East("New Colvin (Morningstar)", Arrays.asList(Neighborhood.Wesley_Cutter_International_Airport, Neighborhood.Camano_Place, Neighborhood.Rosen_Oaks, Neighborhood.Espina));
//	Nonexistant("Nonexistant", Arrays.asList(Neighborhood.Unknown)),
	private String name;
	private List<Neighborhood> neighborhoods;

	private District(String name, List<Neighborhood> neighborhoods) {
		this.name = name;
		this.neighborhoods = neighborhoods;
	}

	@Override
	public String toString() {
		return name;
	}

	public List<Neighborhood> getNeighborhoods() {
		return Collections.unmodifiableList(neighborhoods);
	}
}
