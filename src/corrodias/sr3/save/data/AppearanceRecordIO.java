package corrodias.sr3.save.data;

import java.io.*;

public class AppearanceRecordIO {

	public static void writePlayer(AppearanceRecord record, String filename) throws Exception {
		writePlayer(record, new File(filename));
	}

	public static void writePlayer(AppearanceRecord record, File file) throws Exception {
		try (BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(file))) {
			byte[] bytes = record.getBytes();
			output.write(bytes, 0, bytes.length);
		}
	}

	public static AppearanceRecord readPlayer(String filename) throws Exception {
		return readPlayer(new File(filename));
	}

	public static AppearanceRecord readPlayer(File file) throws Exception {
		AppearanceRecord record;
		try (BufferedInputStream input = new BufferedInputStream(new FileInputStream(file))) {
			byte[] rawRecord = new byte[AppearanceRecord.RECORD_SIZE];
			input.read(rawRecord);
			record = AppearanceRecord.parseBytes(rawRecord);
		}
		return record;
	}

	private AppearanceRecordIO() {
	}
}
