package corrodias.sr3.save.data;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Utils {
	public static BufferedReader openBufferedReaderToStringList(String filename) {
		try {
			return new BufferedReader(new InputStreamReader(new FileInputStream(filename), StandardCharsets.UTF_8));
		} catch (FileNotFoundException ex) {
			//noinspection HardcodedFileSeparator
			InputStream fileStream = ClassLoader.getSystemResourceAsStream("corrodias/sr3/save/data/lists/" + filename);
			return new BufferedReader(new InputStreamReader(fileStream, StandardCharsets.UTF_8));
		}
	}

	public static List<String> getFileLinesAsList(Readable source) {
		List<String> result = new ArrayList<>(100);
		try (Scanner scanner = new Scanner(source)) {
			scanner.useDelimiter("[\\r\\n]+");
			while (scanner.hasNext()) {
				result.add(scanner.next());
			}
		}
		return result;
	}
}
