package corrodias.sr3.save.data;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class AppearanceRecord {

	public byte[] data;
	public static final int RECORD_SIZE = 8788;

	public AppearanceRecord() {
	}

	public static AppearanceRecord parseBytes(byte[] record) {
		AppearanceRecord output = new AppearanceRecord();
		output.data = Arrays.copyOf(record, record.length);
		return output;
	}

	public byte[] getBytes() {
		ByteBuffer output = ByteBuffer.allocate(RECORD_SIZE);
		output.put(data);
		return output.array();
	}

	@Override
	public String toString() {
		return super.toString();
	}
}
