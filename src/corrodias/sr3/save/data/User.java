package corrodias.sr3.save.data;

public class User {
	private String name;
	private String steamid;
	private String savePath;

	public User() {
	}

	private User(User clone) {
		this.name = clone.name;
		this.steamid = clone.steamid;
		this.savePath = clone.savePath;
	}

	public String getName() {
		return name;
	}

	public User setName(String name) {
		this.name = name;
		return this;
	}

	public User withName(String name) {
		User clone = new User(this);
		clone.name = name;
		return clone;
	}

	public String getSteamid() {
		return steamid;
	}

	public User setSteamid(String steamid) {
		this.steamid = steamid;
		return this;
	}

	public User withSteamid(String steamid) {
		User clone = new User(this);
		clone.steamid = steamid;
		return clone;
	}

	public String getSavePath() {
		return savePath;
	}

	public User setSavePath(String savePath) {
		this.savePath = savePath;
		return this;
	}

	public User withSavePath(String savePath) {
		User clone = new User(this);
		clone.savePath = savePath;
		return clone;
	}

	@Override
	public String toString() {
		return "User{" +
				"name='" + name + '\'' +
				", steamid='" + steamid + '\'' +
				", savePath='" + savePath + '\'' +
				'}';
	}
}
