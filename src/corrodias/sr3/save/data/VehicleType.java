package corrodias.sr3.save.data;

public enum VehicleType {

	AUTO("Automobile"),
	BIKE("Motorcycle"),
	HELI("Helicopter"),
	VTOL("VTOL"),
	BOAT("Watercraft"),
	PLANE("Airplane");
	private String displayName;

	private VehicleType(String displayName) {
		this.displayName = displayName;
	}

	public static VehicleType getTypeFor(String name) {
		switch (name) {
			case "Automobile":
				return AUTO;
			case "Motorcycle":
				return BIKE;
			case "Helicopter":
				return HELI;
			case "VTOL":
				return VTOL;
			case "Watercraft":
				return BOAT;
			case "Airplane":
				return PLANE;
			default:
				return null;
		}
	}

	@Override
	public String toString() {
		return displayName;
	}
}
