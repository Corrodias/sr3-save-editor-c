package corrodias.sr3.save.data;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

public class ListReader {

	private static final Pattern COLON_PATTERN = Pattern.compile(":");
	private final List<String> weaponNames;
	private final List<Territory> territories;

	public ListReader() throws Exception {
		weaponNames = loadWeaponNameList();
		territories = loadTerritoryList();
	}

	private static List<String> loadWeaponNameList() throws Exception {
		try (BufferedReader weaponStream = Utils.openBufferedReaderToStringList("weapon_list.txt")) {
			return Utils.getFileLinesAsList(weaponStream);
		}
	}

	private static List<Territory> loadTerritoryList() throws Exception {
		List<Territory> result = new ArrayList<>(100);
		try (BufferedReader territoryStream = Utils.openBufferedReaderToStringList("territory_list.txt")) {
			List<String> territories = Utils.getFileLinesAsList(territoryStream);
			for (String line : territories) {
				String[] linePieces = COLON_PATTERN.split(line);

				String districtName = linePieces[0].replace(" ", "_");
				String neighborhoodName = linePieces[1].replace(" ", "_");
				String territoryName = linePieces[2];

				//noinspection ObjectAllocationInLoop
				Territory territory = new Territory();
				territory.district = District.valueOf(districtName);
				territory.neighborhood = Neighborhood.valueOf(neighborhoodName);
				territory.name = territoryName;

				result.add(territory);
			}
			return result;
		}
	}

	public List<String> getWeaponNames() {
		return Collections.unmodifiableList(weaponNames);
	}

	public List<Territory> getTerritories() {
		return Collections.unmodifiableList(territories);
	}
}
