package corrodias.sr3.save.data;

public class WeaponSlot {

	public WeaponType weaponType;
	public int hashOfCurrentlyChosenWeapon;			// 4 bytes
	public int unknown_1;							// 4 bytes
	public int ammoInClip;							// 4 bytes
	public int ammoInReserve;						// 4 bytes
	public int unknown_2;							// 4 bytes
	public int unknown_3;							// 4 bytes
	public boolean dualWield;						// 1 byte
	public boolean infiniteAmmo;					// 1 byte
	public byte unknown_4;							// 1 byte
	public byte unknown_5;							// 1 byte
}
