package corrodias.sr3.save.data;

import java.io.*;
import java.util.ArrayList;

public class Sr3SaveFileIO {

	public static void writeFile(Sr3SaveFile saveFile, String filename) throws Exception {
		writeFile(saveFile, new File(filename));
	}

	public static void writeFile(Sr3SaveFile saveFile, File file) throws Exception {
		try (BufferedOutputStream output = new BufferedOutputStream(new FileOutputStream(file))) {
			byte[] bytes = saveFile.getBytes();
			output.write(bytes, 0, bytes.length);
		}
	}

	public static Sr3SaveFile readFile(String filename) throws Exception {
		return readFile(new File(filename));
	}

	public static Sr3SaveFile readFile(File file) throws Exception {
		Sr3SaveFile saveFile;
		try (BufferedInputStream input = new BufferedInputStream(new FileInputStream(file))) {
			byte[] rawFile = new byte[Sr3SaveFile.TOTAL_FILE_SIZE];
			input.read(rawFile);
			saveFile = Sr3SaveFile.parseBytes(rawFile);
		}
		return saveFile;
	}

	private static void readGarageRecords(BufferedInputStream input, ArrayList<GarageRecord> result) throws Exception {
		for (int i = 0; i < Sr3SaveFile.GARAGE_SLOTS; i++) {
			byte[] rawRecord = new byte[GarageRecord.RECORD_SIZE];
			input.read(rawRecord);
			GarageRecord cookedRecord = GarageRecord.parseBytes(rawRecord);
			result.add(cookedRecord);
		}
	}

	private Sr3SaveFileIO() {
	}
}
