package corrodias.sr3.garage.manager;

import corrodias.sr3.save.data.GarageRecord;
import corrodias.sr3.save.data.GarageRecordIO;
import corrodias.sr3.save.data.Sr3SaveFile;
import corrodias.sr3.save.data.VehicleType;
import corrodias.sr3.save.editor.Main;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class GarageEditor {

	private Sr3SaveFile saveFile;
	private File lastVehicleFilePath;
	private List<VehicleInfo> vehicleInfo;
	private GarageEditorFrame garageEditorFrame;

	public GarageEditor(Sr3SaveFile saveFile) throws Exception {
		this.saveFile = saveFile;
		vehicleInfo = loadVehicleNameList();
		lastVehicleFilePath = Main.getSavePath();
		setGarageIndices();
	}

	public void displayFrame(JFrame owner) throws Exception {
		garageEditorFrame = new GarageEditorFrame(owner, this);
		garageEditorFrame.setVisible(true);
	}

	public List<GarageRecord> getVehicles() {
		List<GarageRecord> vehicles = new ArrayList<>(Sr3SaveFile.GARAGE_SLOTS);
		for (int i = 0; i < saveFile.garageRecords.size(); i++) {
			GarageRecord record = saveFile.garageRecords.get(i);
			VehicleInfo info = vehicleInfo.get(record.vehicle_id);
			record.name = info.Name;
			record.display_name = info.DisplayName;
			record.type = VehicleType.getTypeFor(info.TypeName);
			vehicles.add(record);
		}
		return vehicles;
	}

	public void swapVehicles(int a, int b) {
		GarageRecord vehicleA = saveFile.garageRecords.get(a - 1);
		GarageRecord vehicleB = saveFile.garageRecords.get(b - 1);
		saveFile.garageRecords.set(a - 1, vehicleB);
		saveFile.garageRecords.set(b - 1, vehicleA);
		vehicleA.index = b;
		vehicleB.index = a;
	}

	public void deleteVehicle(int index) {
		saveFile.garageRecords.remove(index - 1);
		setGarageIndices();
	}

	private void setGarageIndices() {
		int i = 1;
		for (GarageRecord garageRecord : saveFile.garageRecords) {
			garageRecord.index = i++;
		}
	}

	public boolean importVehicle() throws Exception {
		if (saveFile.garageRecords.size() >= Sr3SaveFile.GARAGE_SLOTS) {
			JOptionPane.showMessageDialog(garageEditorFrame, "The garage can only hold " + Sr3SaveFile.GARAGE_SLOTS + " vehicles.", "Garage full", JOptionPane.INFORMATION_MESSAGE);
			return false;
		}
		JFileChooser chooser = new JFileChooser();
		if (lastVehicleFilePath != null) {
			chooser.setCurrentDirectory(lastVehicleFilePath.getParentFile());
		}
		chooser.setFileFilter(new FileNameExtensionFilter("SR3 vehicle export files", "sr3_veh"));
		int returnVal = chooser.showOpenDialog(garageEditorFrame);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			GarageRecord record = GarageRecordIO.readVehicle(chooser.getSelectedFile());
			VehicleInfo info = vehicleInfo.get(record.vehicle_id);
			record.name = info.Name;
			record.display_name = info.DisplayName;
			record.type = VehicleType.getTypeFor(info.TypeName);
			saveFile.garageRecords.add(record);
			setGarageIndices();

			lastVehicleFilePath = chooser.getSelectedFile();

			return true;
		}
		return false;
	}

	public void exportVehicle(int index) throws Exception {
		GarageRecord record = saveFile.garageRecords.get(index - 1);

		JFileChooser chooser = new JFileChooser();
		if (lastVehicleFilePath != null) {
			chooser.setCurrentDirectory(lastVehicleFilePath.getParentFile());
			chooser.setSelectedFile(new File(lastVehicleFilePath.getParent() + File.separator + record.display_name + ".sr3_veh"));
		}
		chooser.setFileFilter(new FileNameExtensionFilter("SR3 vehicle export files", "sr3_veh"));
		int returnVal = chooser.showSaveDialog(garageEditorFrame);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			String filename = chooser.getSelectedFile().getPath();
			if (!filename.endsWith(".sr3_veh")) {
				filename += ".sr3_veh";
			}
			GarageRecordIO.writeVehicle(record, filename);

			lastVehicleFilePath = chooser.getSelectedFile();
		}
	}

	private List<VehicleInfo> loadVehicleNameList() throws Exception {
		BufferedReader vehicleStream = null;
		try {
			try {
				vehicleStream = new BufferedReader(new InputStreamReader(new FileInputStream("vehicle_list.txt"), StandardCharsets.UTF_8));
			} catch (FileNotFoundException ex) {
				vehicleStream = new BufferedReader(new InputStreamReader(ClassLoader.getSystemResourceAsStream("corrodias/sr3/save/data/lists/vehicle_list.txt"), StandardCharsets.UTF_8));
			}
			List<VehicleInfo> result = new ArrayList<>(250);
			String line;
			while ((line = vehicleStream.readLine()) != null) {
				VehicleInfo item = VehicleInfo.parseString(line);
				result.add(item);
			}
			return result;
		} finally {
			try {
				vehicleStream.close();
			} catch (Exception ex2) {
			}
		}
	}
}
