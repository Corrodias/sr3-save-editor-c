package corrodias.sr3.garage.manager;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class VehicleInfoParser {

	private String vehiclesFile;
	private String vehiclesPath;

	public VehicleInfoParser(String vehiclesFile) throws Exception {
		this.vehiclesFile = vehiclesFile;
		this.vehiclesPath = new File(vehiclesFile).getAbsoluteFile().getParent();
	}

	public void outputVehicleInfo() throws Exception {
		XPathFactory xpathFactory = XPathFactory.newInstance();
		XPath xpath = xpathFactory.newXPath();

		List<String> vehicleNames = new ArrayList<>(250);

		try (Reader in = new BufferedReader(new FileReader(vehiclesFile))) {
			InputSource source = new InputSource(in);
			NodeList evaluate = (NodeList) xpath.evaluate("/root/Table/Vehicle/Name", source, XPathConstants.NODESET);
			for (int i = 0; i < evaluate.getLength(); i++) {
				Node item = evaluate.item(i);
				vehicleNames.add(item.getFirstChild().getNodeValue());
			}
		}

		try (Writer out = new BufferedWriter(new FileWriter(vehiclesFile + ".namelist"))) {
			for (String vehicleName : vehicleNames) {
				VehicleInfo vehicleInfo = getVehicleInfoFromFile(vehicleName, xpath);
				String infoString = vehicleInfo.toString();
				out.append(infoString).append(System.lineSeparator());
			}
		}
	}

	private VehicleInfo getVehicleInfoFromFile(String vehicleName, XPath xpath) throws Exception {
		VehicleInfo result = new VehicleInfo();
		result.Name = vehicleName;
		try {
			result.DisplayName = getVehicleDisplayName(vehicleName, xpath);
			result.TypeName = getVehicleTypeName(vehicleName, xpath);
		} catch (FileNotFoundException ex) {
			result.DisplayName = "(" + vehicleName + ")";
			result.TypeName = null;
		}
		return result;
	}

	private String getVehicleDisplayName(String vehicleName, XPath xpath) throws Exception {
		try (Reader in = new BufferedReader(new FileReader(vehiclesPath + File.separator + vehicleName + "_veh.xtbl"))) {
			InputSource source = new InputSource(in);
			NodeList nameNodes = (NodeList) xpath.evaluate("/root/Table/Vehicle/Display_Name", source, XPathConstants.NODESET);
			return nameNodes.item(0).getFirstChild().getNodeValue();
		}
	}

	private String getVehicleTypeName(String vehicleName, XPath xpath) throws Exception {
		try (Reader in = new BufferedReader(new FileReader(vehiclesPath + File.separator + vehicleName + "_veh.xtbl"))) {
			InputSource source = new InputSource(in);
			NodeList typeNodes = (NodeList) xpath.evaluate("/root/Table/Vehicle/Vehicle_Type", source, XPathConstants.NODESET);
			return typeNodes.item(0).getFirstChild().getNextSibling().getNodeName();
		}
	}
}
