package corrodias.sr3.garage.manager;

import java.util.Scanner;
import java.util.regex.Pattern;

public class VehicleInfo {

	public String Name;
	public String DisplayName;
	public String TypeName;
	public static final Pattern tabDelimiterPattern = Pattern.compile("\t");

	@Override
	public String toString() {
		return Name + "\t" + DisplayName + "\t" + TypeName;
	}

	public static VehicleInfo parseString(String vehicleInfo) {
		VehicleInfo result = new VehicleInfo();
		try (final Scanner sc = new Scanner(vehicleInfo)) {
			sc.useDelimiter(tabDelimiterPattern);
			result.Name = sc.next();
			result.DisplayName = sc.next();
			result.TypeName = sc.next();
		}
		return result;
	}
}
