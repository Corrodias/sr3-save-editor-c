package corrodias.sr3.garage.manager;

import com.codeidol.java.swing.lists_and_combos.reorder_a_jlist_with_drag_and_drop.ReorderableJList;
import corrodias.sr3.save.data.GarageRecord;
import corrodias.sr3.save.data.Sr3SaveFile;
import corrodias.sr3.save.data.VehicleType;
import corrodias.sr3.save.data.VehicleTypeFilter;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

@SuppressWarnings("serial")
public class GarageEditorFrame extends JDialog {

	private JList<GarageRecord> vehicleList;
	private DefaultListModel<GarageRecord> vehicleListModel;
	private JList<VehicleTypeFilter> vehicleTypeFilterList;
	private DefaultListModel<VehicleTypeFilter> vehicleTypeFilterListModel;
	private VehicleListColoredCellRenderer vehicleListColoredCellRenderer;
	private JButton upButton;
	private JButton downButton;
	private JButton deleteButton;
	private JButton importButton;
	private JButton exportButton;
	private JTextField autoColorField;
	private JTextField bikeColorField;
	private JTextField heliColorField;
	private JTextField vtolColorField;
	private JTextField boatColorField;
	private JTextField planeColorField;
	private JCheckBox useColor;
	private GarageEditor garageEditor;

	public GarageEditorFrame(JFrame owner, GarageEditor main) {
		super(owner, "Garage Editor", ModalityType.APPLICATION_MODAL);

		this.garageEditor = main;

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setSize(new Dimension(1024, 768));
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));

		JPanel vehiclePanel = new JPanel();
		vehiclePanel.setLayout(new BoxLayout(vehiclePanel, BoxLayout.X_AXIS));
		getContentPane().add(vehiclePanel);


		vehicleListModel = new DefaultListModel<>();
//		vehicleList = new JList<>(vehicleListModel);
//		vehicleList.setLayoutOrientation(JList.VERTICAL);
		vehicleList = new VehicleJList<>();
		vehicleListColoredCellRenderer = new VehicleListColoredCellRenderer(vehicleList.getCellRenderer());
		vehicleList.setCellRenderer(vehicleListColoredCellRenderer);

		vehicleList.setModel(vehicleListModel);
		vehicleList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

		JScrollPane vehiclePane = new JScrollPane(vehicleList);
		vehiclePane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		vehiclePane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		vehiclePanel.add(vehiclePane);

		upButton = new JButton("<html><center>Move<br/>Up</center></html>");
		downButton = new JButton("<html><center>Move<br/>Down</center></html>");
		deleteButton = new JButton("Delete");
		importButton = new JButton("Import");
		exportButton = new JButton("Export");

		int width = 100;
		int height = (int) upButton.getSize().getHeight();
		Dimension buttonSize = new Dimension(width, height);
		upButton.setMinimumSize(buttonSize);
		upButton.setMaximumSize(buttonSize);
		downButton.setMinimumSize(buttonSize);
		downButton.setMaximumSize(buttonSize);
		deleteButton.setMinimumSize(buttonSize);
		deleteButton.setMaximumSize(buttonSize);
		importButton.setMinimumSize(buttonSize);
		importButton.setMaximumSize(buttonSize);
		exportButton.setMinimumSize(buttonSize);
		exportButton.setMaximumSize(buttonSize);

		upButton.addActionListener(new UpButtonListener());
		downButton.addActionListener(new DownButtonListener());
		deleteButton.addActionListener(new DeleteButtonListener());
		importButton.addActionListener(new ImportButtonListener());
		exportButton.addActionListener(new ExportButtonListener());

		vehicleTypeFilterListModel = new DefaultListModel<>();
		for (VehicleTypeFilter vehicleTypeFilter : VehicleTypeFilter.values()) {
			vehicleTypeFilterListModel.addElement(vehicleTypeFilter);
		}

		vehicleTypeFilterList = new JList<>(vehicleTypeFilterListModel);
		vehicleTypeFilterList.setSelectedIndex(0);
		vehicleTypeFilterList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		vehicleTypeFilterList.addListSelectionListener(new FilterListSelectionListener());

		JScrollPane typesPane = new JScrollPane(vehicleTypeFilterList);
		typesPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		typesPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		typesPane.setMinimumSize(buttonSize);
		typesPane.setMaximumSize(buttonSize);
		typesPane.setAlignmentX(LEFT_ALIGNMENT); // no idea why this is necessary

		autoColorField = new JTextField("#FFE0E0");
		bikeColorField = new JTextField("#FFEFC0");
		heliColorField = new JTextField("#FCFFC0");
		vtolColorField = new JTextField("#E0FFE0");
		boatColorField = new JTextField("#E0E0FF");
		planeColorField = new JTextField("#FFFFFF");
		vehicleListColoredCellRenderer.setColor(VehicleType.AUTO, autoColorField.getText());
		vehicleListColoredCellRenderer.setColor(VehicleType.BIKE, bikeColorField.getText());
		vehicleListColoredCellRenderer.setColor(VehicleType.HELI, heliColorField.getText());
		vehicleListColoredCellRenderer.setColor(VehicleType.VTOL, vtolColorField.getText());
		vehicleListColoredCellRenderer.setColor(VehicleType.BOAT, boatColorField.getText());
		vehicleListColoredCellRenderer.setColor(VehicleType.PLANE, planeColorField.getText());
		autoColorField.getDocument().addDocumentListener(new ColorFieldListener(VehicleType.AUTO, autoColorField));
		bikeColorField.getDocument().addDocumentListener(new ColorFieldListener(VehicleType.BIKE, bikeColorField));
		heliColorField.getDocument().addDocumentListener(new ColorFieldListener(VehicleType.HELI, heliColorField));
		vtolColorField.getDocument().addDocumentListener(new ColorFieldListener(VehicleType.VTOL, vtolColorField));
		boatColorField.getDocument().addDocumentListener(new ColorFieldListener(VehicleType.BOAT, boatColorField));
		boatColorField.getDocument().addDocumentListener(new ColorFieldListener(VehicleType.PLANE, planeColorField));

		width = 120;
		height = (int) autoColorField.getSize().getHeight();
		Dimension textFieldSize = new Dimension(width, height);
		autoColorField.setMaximumSize(textFieldSize);
		bikeColorField.setMaximumSize(textFieldSize);
		heliColorField.setMaximumSize(textFieldSize);
		vtolColorField.setMaximumSize(textFieldSize);
		boatColorField.setMaximumSize(textFieldSize);
		planeColorField.setMaximumSize(textFieldSize);

		useColor = new JCheckBox("Use colors");
		useColor.setSelected(true);
		useColor.addActionListener(new UseColorCheckboxListener());

		Dimension spacer = new Dimension(0, 10);
		Dimension miniSpacer = new Dimension(0, 4);
		JPanel buttonPanel = new JPanel();
		buttonPanel.setBorder(new EmptyBorder(4, 4, 4, 4));
		buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.Y_AXIS));
		buttonPanel.add(upButton);
		buttonPanel.add(downButton);
		buttonPanel.add(Box.createRigidArea(spacer));
		buttonPanel.add(deleteButton);
		buttonPanel.add(Box.createRigidArea(spacer));
		buttonPanel.add(importButton);
		buttonPanel.add(exportButton);
		buttonPanel.add(Box.createRigidArea(spacer));
		buttonPanel.add(new JLabel("Filter:"));
		buttonPanel.add(typesPane);
		buttonPanel.add(Box.createRigidArea(spacer));
		buttonPanel.add(new JLabel("Auto color:"));
		buttonPanel.add(autoColorField);
		buttonPanel.add(Box.createRigidArea(miniSpacer));
		buttonPanel.add(new JLabel("Bike color:"));
		buttonPanel.add(bikeColorField);
		buttonPanel.add(Box.createRigidArea(miniSpacer));
		buttonPanel.add(new JLabel("Heli color:"));
		buttonPanel.add(heliColorField);
		buttonPanel.add(Box.createRigidArea(miniSpacer));
		buttonPanel.add(new JLabel("VTOL color:"));
		buttonPanel.add(vtolColorField);
		buttonPanel.add(Box.createRigidArea(miniSpacer));
		buttonPanel.add(new JLabel("Boat color:"));
		buttonPanel.add(boatColorField);
		buttonPanel.add(Box.createRigidArea(miniSpacer));
		buttonPanel.add(new JLabel("Plane color:"));
		buttonPanel.add(planeColorField);
		buttonPanel.add(Box.createRigidArea(miniSpacer));
		buttonPanel.add(useColor);
		buttonPanel.add(Box.createVerticalGlue());
		getContentPane().add(buttonPanel);

		addWindowListener(new MainWindowListener());
	}

	private class MainWindowListener implements WindowListener {

		@Override
		public void windowOpened(WindowEvent e) {
			try {
				readColorsFromFile();
				updateVehicleList();
			} catch (Exception ex) {
				Logger.getLogger(GarageEditor.class.getName()).log(Level.SEVERE, null, ex);
			}
		}

		@Override
		public void windowClosing(WindowEvent e) {
			try {
				writeColorsToFile();
			} catch (Exception ex) {
				Logger.getLogger(GarageEditor.class.getName()).log(Level.SEVERE, null, ex);
			}
		}

		@Override
		public void windowClosed(WindowEvent e) {
		}

		@Override
		public void windowIconified(WindowEvent e) {
		}

		@Override
		public void windowDeiconified(WindowEvent e) {
		}

		@Override
		public void windowActivated(WindowEvent e) {
		}

		@Override
		public void windowDeactivated(WindowEvent e) {
		}
	}

	private void readColorsFromFile() throws Exception {
		List<String> colors = new ArrayList<>(5);
		try (BufferedReader in = new BufferedReader(new FileReader("colors.txt"))) {
			String line;
			while ((line = in.readLine()) != null) {
				colors.add(line);
			}
		}
		setColors(colors);
	}

	private void writeColorsToFile() throws Exception {
		try (BufferedWriter out = new BufferedWriter(new FileWriter("colors.txt"))) {
			for (String color : getColors()) {
				out.write(color);
				out.newLine();
			}
		}
	}

	private void setTitle(int numberOfVehicles, int totalVehicles) {
		setTitle("Garage Editor (" + numberOfVehicles + "/" + totalVehicles + "/" + Sr3SaveFile.GARAGE_SLOTS + ")");
	}

	public DefaultListModel<GarageRecord> getVehicleListModel() {
		return vehicleListModel;
	}

	public void updateVehicleList() {
		vehicleListModel.clear();

		List<GarageRecord> vehicles = garageEditor.getVehicles();

		int filterIndex = vehicleTypeFilterList.getSelectedIndex();
		VehicleTypeFilter typeFilter = filterIndex >= 0 ? vehicleTypeFilterListModel.get(filterIndex) : null;
		for (GarageRecord garageRecord : vehicles) {
			if (typeFilter != null && typeFilter.accept(garageRecord.type)) {
				vehicleListModel.addElement(garageRecord);
			}
		}

		setTitle(vehicleListModel.size(), vehicles.size());
	}

	private class UpButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			int[] indices = vehicleList.getSelectedIndices();
			if (indices.length > 0 && indices[0] >= 1) {
				for (int i = 0; i < indices.length; i++) {
					int index = indices[i];

					int upper = index - 1;
					int lower = index;
					swapVehicles(upper, lower);

					indices[i] = index - 1;
				}
				vehicleList.setSelectedIndices(indices);
			}
		}
	}

	private class DownButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			int[] indices = vehicleList.getSelectedIndices();
			if (indices.length > 0 && indices[indices.length - 1] < vehicleListModel.size() - 1) {
				for (int i = indices.length - 1; i >= 0; i--) {
					int index = indices[i];

					int upper = index;
					int lower = index + 1;
					swapVehicles(upper, lower);

					indices[i] = index + 1;
				}
				vehicleList.setSelectedIndices(indices);
			}
		}
	}

	private void swapVehicles(int a, int b) {
		GarageRecord vehicleA = vehicleListModel.get(a);
		GarageRecord vehicleB = vehicleListModel.get(b);
		vehicleListModel.set(a, vehicleB);
		vehicleListModel.set(b, vehicleA);
		garageEditor.swapVehicles(vehicleA.index, vehicleB.index);
	}

	private class DeleteButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (vehicleList.getSelectedIndex() >= 0) {
				int index = vehicleList.getSelectedIndex();
				garageEditor.deleteVehicle(vehicleListModel.get(index).index);

				vehicleListModel.remove(index);
				updateVehicleList();
			}
		}
	}

	private class ImportButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				if (garageEditor.importVehicle()) {
					updateVehicleList();
					vehicleList.setSelectedIndex(vehicleListModel.size() - 1); // scroll to the bottom
				}
			} catch (Exception ex) {
				Logger.getLogger(GarageEditor.class.getName()).log(Level.SEVERE, null, ex);
				JOptionPane.showMessageDialog(GarageEditorFrame.this, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private class ExportButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				int[] indices = vehicleList.getSelectedIndices();
				if (indices.length > 0) {
					for (int i = 0; i < indices.length; i++) {
						int index = indices[i];

						GarageRecord vehicle = vehicleListModel.get(index);
						garageEditor.exportVehicle(vehicle.index);
					}
				}
			} catch (Exception ex) {
				Logger.getLogger(GarageEditor.class.getName()).log(Level.SEVERE, null, ex);
				JOptionPane.showMessageDialog(GarageEditorFrame.this, ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private class FilterListSelectionListener implements ListSelectionListener {

		@Override
		public void valueChanged(ListSelectionEvent e) {
			updateVehicleList();
		}
	}

	private class ColorFieldListener implements DocumentListener {

		private VehicleType vehicleType;
		private JTextField targetField;

		ColorFieldListener(VehicleType vehicleType, JTextField targetField) {
			super();
			this.vehicleType = vehicleType;
			this.targetField = targetField;
		}

		@Override
		public void insertUpdate(DocumentEvent e) {
			vehicleListColoredCellRenderer.setColor(vehicleType, targetField.getText());
			vehicleList.repaint();
		}

		@Override
		public void removeUpdate(DocumentEvent e) {
			vehicleListColoredCellRenderer.setColor(vehicleType, targetField.getText());
			vehicleList.repaint();
		}

		@Override
		public void changedUpdate(DocumentEvent e) {
			vehicleListColoredCellRenderer.setColor(vehicleType, targetField.getText());
			vehicleList.repaint();
		}
	}

	private class UseColorCheckboxListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			vehicleList.repaint();
		}
	}

	private void setUiEnabledAfterLoading(boolean enabled) {
		upButton.setEnabled(enabled);
		downButton.setEnabled(enabled);
		deleteButton.setEnabled(enabled);
		importButton.setEnabled(enabled);
		exportButton.setEnabled(enabled);
		vehicleTypeFilterList.setEnabled(enabled);
	}

	private class VehicleJList<E> extends ReorderableJList<E> {

		@Override
		public void moveItem(int sourceIndex, int targetIndex, Object draggedItem) {
			int index = sourceIndex;
			while (index < targetIndex) {
				int upper = index;
				int lower = index + 1;
				swapVehicles(upper, lower);
				index = lower;
			}
			while (index > targetIndex) {
				int upper = index - 1;
				int lower = index;
				swapVehicles(upper, lower);
				index = upper;
			}
			vehicleList.setSelectedIndex(targetIndex);
		}
	}

	private class VehicleListColoredCellRenderer extends JLabel implements ListCellRenderer<GarageRecord> {

		//final static ImageIcon longIcon = new ImageIcon("long.gif");
		//final static ImageIcon shortIcon = new ImageIcon("short.gif");
		Map<VehicleType, Color> vehicleTypeColors;
		ListCellRenderer delegate;

		VehicleListColoredCellRenderer(ListCellRenderer delegate) {
			vehicleTypeColors = new EnumMap<>(VehicleType.class);
			setBorder(new EmptyBorder(2, 2, 2, 2));
			this.delegate = delegate;
		}

		public void setColor(VehicleType vehicleType, String color) {
			try {
				Color decode = Color.decode(color);
				vehicleTypeColors.put(vehicleType, decode);
			} catch (NumberFormatException ex) {
				vehicleTypeColors.remove(vehicleType);
			}
		}
		// This is the only method defined by ListCellRenderer.
		// We just reconfigure the JLabel each time we're called.

		@Override
		public Component getListCellRendererComponent(
				JList<? extends GarageRecord> list, // the list
				GarageRecord value, // value to display
				int index, // cell index
				boolean isSelected, // is the cell selected
				boolean cellHasFocus) // does the cell have focus
		{
			Component c = delegate.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);

			String s = value.toString();
			setText(s);
			//setIcon((s.length() > 10) ? longIcon : shortIcon);
			if (isSelected) {
				c.setBackground(list.getSelectionBackground());
				c.setForeground(list.getSelectionForeground());
			} else {
				c.setBackground(list.getBackground());
				c.setForeground(list.getForeground());
				Color typeColor = vehicleTypeColors.get(value.type);
				if (typeColor != null && useColor.isSelected()) {
					c.setBackground(typeColor);
				}
			}
			c.setEnabled(list.isEnabled());
			c.setFont(list.getFont());
			setOpaque(true);
			return c;
		}
	}

	public List<String> getColors() {
		List<String> result = new ArrayList<>(5);
		result.add(autoColorField.getText());
		result.add(bikeColorField.getText());
		result.add(heliColorField.getText());
		result.add(vtolColorField.getText());
		result.add(boatColorField.getText());
		result.add(planeColorField.getText());
		result.add(Boolean.toString(useColor.isSelected()));
		return result;
	}

	public void setColors(List<String> colors) {
		autoColorField.setText(colors.get(0) == null ? "" : colors.get(0));
		bikeColorField.setText(colors.get(1) == null ? "" : colors.get(1));
		heliColorField.setText(colors.get(2) == null ? "" : colors.get(2));
		vtolColorField.setText(colors.get(3) == null ? "" : colors.get(3));
		boatColorField.setText(colors.get(4) == null ? "" : colors.get(4));
		planeColorField.setText(colors.get(5) == null ? "" : colors.get(5));
		useColor.setSelected(Boolean.valueOf(colors.get(6)));
	}
}
