package com.codeidol.java.swing.lists_and_combos.reorder_a_jlist_with_drag_and_drop;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.*;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JList;

/**
 * Modified by Corrodias from source at
 * http://codeidol.com/java/swing/Lists-and-Combos/Reorder-a-JList-with-Drag-and-Drop/
 *
 * @author Corrodias
 * @param <E>
 */
@SuppressWarnings("serial")
public class ReorderableJList<E> extends JList<E>
		implements DragSourceListener, DropTargetListener, DragGestureListener {

	private static DataFlavor localObjectFlavor;

	static {
		try {
			localObjectFlavor =
					new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType);
		} catch (ClassNotFoundException cnfe) {
			Logger.getLogger(ReorderableJList.class.getName()).log(Level.WARNING, null, cnfe);
		}
	}
	private static DataFlavor[] supportedFlavors = {localObjectFlavor};
	private DragSource dragSource;
	private DropTarget dropTarget;
	private Object dropTargetCell;
	private Object adjacentCell;
	private boolean dropTargetBelowCell;
	private int draggedIndex = -1;

	public ReorderableJList() {
		super();
		setCellRenderer(new ReorderableListCellRenderer());
		setModel(new DefaultListModel<E>());
		dragSource = new DragSource();
		DragGestureRecognizer dgr =
				dragSource.createDefaultDragGestureRecognizer(this,
				DnDConstants.ACTION_MOVE,
				this);
		dropTarget = new DropTarget(this, this);
	}

	// DragGestureListener
	@Override
	public void dragGestureRecognized(DragGestureEvent dge) {
		// find object at this x,y
		Point clickPoint = dge.getDragOrigin();
		int index = locationToIndex(clickPoint);
		if (index == -1) {
			return;
		}
		Object target = getModel().getElementAt(index);
		Transferable trans = new RJLTransferable(target);
		draggedIndex = index;
		dragSource.startDrag(dge, Cursor.getDefaultCursor(),
				trans, this);
	}
	// DragSourceListener events

	@Override
	public void dragDropEnd(DragSourceDropEvent dsde) {
		dropTargetCell = null;
		adjacentCell = null;
		draggedIndex = -1;
		repaint();
	}

	@Override
	public void dragEnter(DragSourceDragEvent dsde) {
	}

	@Override
	public void dragExit(DragSourceEvent dse) {
	}

	@Override
	public void dragOver(DragSourceDragEvent dsde) {
	}

	@Override
	public void dragOver(DropTargetDragEvent dtde) {
		// figure out which cell it's over, no drag to self    
		if (dtde.getSource() != dropTarget) {
			dtde.rejectDrag();
		}

		Point dropPoint = dtde.getLocation();
		int originalIndex = locationToIndex(dropPoint);
		Rectangle cellBounds = getCellBounds(originalIndex, originalIndex);
		double cellTop = cellBounds.getMinY();
		double cellHeight = cellBounds.getHeight();
		double positionWithinCell = dropPoint.getY() - cellTop;
		double percentageThroughCell = positionWithinCell / cellHeight;

		int index = originalIndex;
		if (percentageThroughCell > 0.50) {
			dropTargetBelowCell = true;
		} else {
			dropTargetBelowCell = false;
		}

		if (index == -1) {
			dropTargetCell = null;
			adjacentCell = null;
		} else {
			dropTargetCell = getModel().getElementAt(index);
			int adjacentIndex = dropTargetBelowCell ? index + 1 : index - 1;
			if (adjacentIndex >= 0 && adjacentIndex < getModel().getSize()) {
				adjacentCell = getModel().getElementAt(adjacentIndex);
			} else {
				adjacentCell = null;
			}
		}
		repaint();
	}

	@Override
	public void dropActionChanged(DragSourceDragEvent dsde) {
	}
	// DropTargetListener events

	@Override
	public void dragEnter(DropTargetDragEvent dtde) {
		if (dtde.getSource() != dropTarget) {
			dtde.rejectDrag();
		} else {
			dtde.acceptDrag(DnDConstants.ACTION_COPY_OR_MOVE);
		}
	}

	@Override
	public void dragExit(DropTargetEvent dte) {
	}

	@Override
	public void dropActionChanged(DropTargetDragEvent dtde) {
	}

	@Override
	public void drop(DropTargetDropEvent dtde) {
		if (dtde.getSource() != dropTarget) {
			dtde.rejectDrop();
			return;
		}

		Point dropPoint = dtde.getLocation();
		int originalIndex = locationToIndex(dropPoint);
		Rectangle cellBounds = getCellBounds(originalIndex, originalIndex);
		double cellTop = cellBounds.getMinY();
		double cellHeight = cellBounds.getHeight();
		double positionWithinCell = dropPoint.getY() - cellTop;
		double percentageThroughCell = positionWithinCell / cellHeight;

		int index = originalIndex;
		if (percentageThroughCell > 0.50) {
			index += 1;
		}

		boolean dropped = false;
		try {
			if ((index == -1) || (index == draggedIndex)) {
				dtde.rejectDrop();
				return;
			}
			dtde.acceptDrop(DnDConstants.ACTION_MOVE);
			E dragged =
					(E) dtde.getTransferable().getTransferData(localObjectFlavor);
			// move items - note that indicies for insert will 
			// change if [removed] source was before target 
			boolean sourceBeforeTarget = (draggedIndex < index);
			moveItem(draggedIndex, (sourceBeforeTarget ? index - 1 : index), dragged);
			dropped = true;
		} catch (Exception e) {
			Logger.getLogger(ReorderableJList.class.getName()).log(Level.SEVERE, null, e);
		}
		dtde.dropComplete(dropped);
	}

	public void moveItem(int sourceIndex, int targetIndex, E draggedItem) {
		DefaultListModel<E> mod = (DefaultListModel<E>) getModel();
		mod.remove(draggedIndex);
		mod.add(targetIndex, draggedItem);
	}

	private class RJLTransferable implements Transferable {

		Object object;

		RJLTransferable(Object o) {
			object = o;
		}

		@Override
		public Object getTransferData(DataFlavor df)
				throws UnsupportedFlavorException, IOException {
			if (isDataFlavorSupported(df)) {
				return object;
			} else {
				throw new UnsupportedFlavorException(df);
			}
		}

		@Override
		public boolean isDataFlavorSupported(DataFlavor df) {
			return (df.equals(localObjectFlavor));
		}

		@Override
		public DataFlavor[] getTransferDataFlavors() {
			return supportedFlavors;
		}
	}

	private class ReorderableListCellRenderer extends DefaultListCellRenderer {

		boolean isTargetCell;
		boolean isAdjacentCell;
		boolean isLastItem;

		ReorderableListCellRenderer() {
			super();
		}

		@Override
		public Component getListCellRendererComponent(JList list,
				Object value,
				int index,
				boolean isSelected, boolean hasFocus) {
			isTargetCell = (value == dropTargetCell);
			isAdjacentCell = (value == adjacentCell);
			isLastItem = (index == list.getModel().getSize() - 1);
			boolean showSelected = isSelected
					& (dropTargetCell == null);
			return super.getListCellRendererComponent(list, value,
					index, showSelected,
					hasFocus);
		}

		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.setColor(Color.black);

			if (isTargetCell) {
				if (dropTargetBelowCell) {
					drawLineOnBottom(g);
				} else {
					drawLineOnTop(g);
				}
			}

			if (isAdjacentCell) {
				if (dropTargetBelowCell) {
					drawLineOnTop(g);
				} else {
					drawLineOnBottom(g);
				}
			}
		}

		private void drawLineOnTop(Graphics g) {
			g.drawLine(0, 0, getSize().width, 0);
		}

		private void drawLineOnBottom(Graphics g) {
			g.drawLine(0, getSize().height - 1, getSize().width, getSize().height - 1);
		}
	}
}
